## PORTAIL MULTI ACCÉS
Ce portail permet d'interroger simultanément plusieurs bases de données inventaire déclarées dans le logiciel.
- Les bases de type DBAconit sont interrogées à travers le protocole XMLRPC.
- Pour les autres, le logiciel remplit le forumaire de recherche HTML et interprète l'écran HTML de résultats retourné par la bse interrogée.

## Documentation
La documentation se trouve dans le dossier [Documentation](https://db.aconit.org/documentation/),
Il s'agit du mémoire de stage d'Arnaud Adamy qui décrit bien les choix initiaux, la réalisation et comprte des annexes techniques précises. 
Voir [Memoire_portail_multi_acces.pdf](https://db.aconit.org/documentation/Memoire_portail_multi_acces.pdf)

## Crédit
Le portail est entièrement la réalisation de M. Arnaud Adamy, lors d'un stage dans les locaux d'ACONIT
