<?php

function displayAddP1 ($action)
{
    echo '    <div class="frame3dtitle">
      <h1>Ajout d\'une entrée</h1>
      <h2>Choix du type d\'entrée</h2>
    </div>
    
    <form action="" method="get">
      <div class="frame3d"><fieldset>
        <legend>Choix du type d\'entrée</legend>
        <table><tbody>
          <tr>
            <td><label for="typeofentry">Type d\'entrée*</label></td>
            <td>
              <select name="typeofentry" id="typeofentry">
                <option value="ta">Analyse textuelle</option>
                <option value="ws">Service Web</option>
              </select>
            </td>
          </tr>
          <tr>
            <td><label for="key">Clé d\'index*</label></td>
            <td><input type="text" name="key" value="" /></td>
          </tr>
          <input type="hidden" name="action" value="'.$action.'" />
        </tbody></table>
      </fieldset></div>
      <div class="frame3d">
        <input type="submit" value="Continuer >" />
      </div>
    </form>
';
}

function displayAddP2 ($typeofentry, $key, $types, $action)
{
    echo '    <div class="frame3dtitle">
      <h1>Ajout d\'une entrée</h1>
      <h2>Information sur l\'entrée <i>"'.$key.'"</i></h2>
    </div>

    <form action="" method="get">

      <div class="frame3d"><fieldset>
        <legend>Informations générales</legend>
        <table><tbody>
          <tr>
            <td><label for="name">Nom de l\'entrée*</label></td>
            <td><input type="text" name="name" id="name" /></td>
          </tr>
          <tr>
            <td><label for="infos">Informations sur l\'entrée</label></td>
            <td><textarea name="infos" id="infos" rows="3" cols="30"></textarea></td>
          </tr>
          <tr>
            <td><label for="theme">Thème*</label></td>
            <td>
              <select name="theme" multiple="multiple" size="3" id="theme">
                <option value="informatique">Informatique</option>
                <option value="pstc">PSTC</option>
              </select>
            </td>
          </tr>
          <tr>
            <td><label for="website">Site web*</label></td>
            <td><input type="text" name="website" value="" id="website" /></td>
          </tr>
          <tr>
            <td><label for="domain">Domaine d\'hébergement*</label></td>
            <td><input type="text" name="domain" value="" id="domain" /></td>
          </tr>
        </tbody></table>
    
        <input type="hidden" name="action" value="'.$action.'" />
        <input type="hidden" name="typeofentry" value="'.$typeofentry.'" />
        <input type="hidden" name="key" value="'.$key.'" />
      </fieldset></div>
';

    // Analyse textuelle
    if ($typeofentry == $types[0])
    {
      echo '    <div class="frame3d"><fieldset>
      <legend>Informations de recherche</legend>
      <table><tbody>

        <tr>
          <td><label for="search_path">Chemin absolu*</label></td>
          <td><input type="text" name="search_path" value="" id="search_path" /></td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Paramètres GET</legend>
              <table><tbody>
                <tr>
                  <td><label for="search_get_name">#1</label></td>
                  <td><input type="text" name="search_get_name" value="" id="search_get_name" /></td>
                  <td><label for="search_get_value">=</label></td>
                  <td><input type="text" name="search_get_value" value="" id="search_get_value" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Paramètres POST</legend>
              <table><tbody>
                <tr>
                  <td><label for="search_post_name">#1</label></td>
                  <td><input type="text" name="search_post_name" value="" id="search_post_name" /></td>
                  <td><label for="search_post_value">=</label></td>
                  <td><input type="text" name="search_post_value" value="" id="search_post_value" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Filtres d\'analyse*</legend>
              <fieldset>
                <legend>#1<button>+</button></legend>
                <table><tbody>
                  <tr>
                    <td><label for="search_filter_regex">Expression rationnelle PERL*</label></td>
                    <td><input type="text" name="search_filter_regex" value="" id="search_filter_regex" /></td>
                  </tr>
                  <tr>
                    <td><label for="search_filter_template">Motif de structuration*</label></td>
                    <td><input type="text" name="search_filter_template" value="" id="search_filter_template" /></td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <fieldset>
                        <legend>Chaînes de caractères à exclure*</legend>
                        <table><tbody>
                          <tr>
                            <td><label for="link_get_name">#1</label></td>
                            <td><input type="text" name="link_get_name" value="" id="link_get_name" /></td>
                            <td><button>+</button></td>
                          </tr>
                        </tbody></table>
                      </fieldset>
                    </td>
                  </tr>
                </tbody></table>
              </fieldset>
            </fieldset>
          </td>
        </tr>

        <tr>
          <td colspan="2">
          <fieldset>
            <legend>Plusieurs pages à consulter <select><option value="no">Non</option><option value="yes">Oui</option></select>*</legend>
            <table><tbody>
              <tr>
                <td><label for="search_repeat_first">Numéro de la 1ère page*</label></td>
                <td><input type="text" name="search_repeat_first" value="" id="search_repeat_first" /></td>
              </tr>
              <tr>
                <td><label for="search_repeat_per_page">Nombre d\'éléments par page*</label></td>
                <td><input type="text" name="search_repeat_per_page" value="" id="search_repeat_per_page" /></td>
              </tr>
              <tr>
                <td><label for="search_repeat_regex">Expression rationelle PERL*</label></td>
                <td><input type="text" name="search_repeat_regex" value="" id="search_repeat_regex" /></td>
              </tr>
            </tbody></table>
          </fieldset>
          </td>
        </tr>

      </tbody></table>
    </fieldset></div>
      
    <div class="frame3d"><fieldset>
      <legend>Informations de construction des liens hypertextes</legend>
      <table><tbody>

        <tr>
          <td><label for="link_path">Chemin de la fiche*</label></td>
          <td><input type="text" name="link_path" value="" id="link_path"/></td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Paramètres GET</legend>
              <table><tbody>
                <tr>
                  <td><label for="link_get_name">#1</label></td>
                  <td><input type="text" name="link_get_name" value="" id="link_get_name" /></td>
                  <td><label for="link_get_value">=</label></td>
                  <td><input type="text" name="link_get_value" value="" id="link_get_value" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Paramètres POST</legend>
              <table><tbody>
                <tr>
                  <td><label for="link_post_name">#1</label></td>
                  <td><input type="text" name="link_post_name" value="" id="link_post_name" /></td>
                  <td><label for="link_post_value">=</label></td>
                  <td><input type="text" name="link_post_value" value="" id="link_post_value" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>
      
        <tr>
          <td><label for="link_filter">Expression rationnelle PERL*</label></td>
          <td><input type="text" name="link_filter" value="" id="link_filter" /></td>
        </tr>

      </tbody></table>
    </fieldset></div>
';
    }
    // WebService
    elseif ($typeofentry == $types[1])
    {
      echo '    <div class="frame3d"><fieldset>
      <legend>Informations sur le serveur</legend>
      <table><tbody>

        <tr>
          <td><label for="server_path">Chemin du serveur*</label></td>
          <td><input type="text" name="server_path" value="" id="server_path"/></td>
        </tr>

        <tr>
          <td><label for="server_port">Port</label></td>
          <td><input type="text" name="server_port" value="" id="server_port"/></td>
        </tr>

        <tr>
          <td><label for="server_login">Utilisateur</label></td>
          <td><input type="text" name="server_login" value="" id="server_login"/></td>
        </tr>

        <tr>
          <td><label for="server_password">Mot-de-passe</label></td>
          <td><input type="password" name="server_password" value="" id="server_password"/></td>
        </tr>

      </tbody></table>
    </fieldset></div>

    <div class="frame3d"><fieldset>
      <legend>Paramètres du message</legend>
      <table><tbody>

      <tr>
        <td><label for="parameters_methodname">Fonction d\'appel*</label></td>
        <td><input type="text" name="parameters_methodname" value="" id="parameters_methodname" /></td>
      </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Valeurs du message*</legend>
              <table><tbody>
                <tr>
                  <td>
                    <fieldset>
                      <legend>#1<button>+</button></legend>
                      <table><tbody>
                        <tr>
                          <td><span>value</span></td>
                          <td><label for="parameters_values_value">=</label></td>
                          <td><input type="text" name="parameters_values_value" value="" id="parameters_values_value" /></td>
                        </tr>
                        <tr>
                          <td><span>type</span></td>
                          <td><label for="parameters_values_type">=</label></td>
                          <td>
                            <select name="parameters_values_type" id="parameters_values_type">
                              <option value="string">Chaîne de caractère</option>
                              <option value="integer">Entier</option>
                              <option value="float">Réel</option>
                              <option value="boolean">Booléen</option>
                            </select>
                          </td>
                        </tr>
                      </tbody></table>
                    </fieldset>
                  </td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>
      
        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Correspondance des éléments</legend>
              <table><tbody>
                <tr>
                  <td><label for="elements_link">Lien*</label></td>
                  <td><input type="text" name="elements_link" value="" id="elements_link" /></td>
                </tr>
                <tr>
                  <td><label for="elements_reference">Référence*</label></td>
                  <td><input type="text" name="elements_reference" value="" id="elements_reference" /></td>
                </tr>
                <tr>
                  <td><label for="elements_type">Type d\'objet*</label></td>
                  <td><input type="text" name="elements_type" value="" id="elements_type" /></td>
                </tr>
                <tr>
                  <td><label for="elements_name">Désignation*</label></td>
                  <td><input type="text" name="elements_name" value="" id="elements_name" /></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>En-tête du résultat*</legend>
              <table><tbody>
                <tr>
                  <td><label for="elements_resultsname">#1</label></td>
                  <td><input type="text" name="elements_resultsname" value="" id="elements_resultsname" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

      </tbody></table>
    </fieldset></div>
      
    <div class="frame3d"><fieldset>
      <legend>Informations de construction des liens hypertextes</legend>
      <table><tbody>

        <tr>
          <td><label for="link_path">Chemin de la fiche*</label></td>
          <td><input type="text" name="link_path" value="" id="link_path"/></td>
        </tr>

        <tr>
          <td colspan="2">
            <fieldset>
              <legend>Paramètres GET*</legend>
              <table><tbody>
                <tr>
                  <td><label for="link_get_name">#1</label></td>
                  <td><input type="text" name="link_get_name" value="" id="link_get_name" /></td>
                  <td><label for="link_get_value">=</label></td>
                  <td><input type="text" name="link_get_value" value="" id="link_get_value" /></td>
                  <td><button>+</button></td>
                </tr>
              </tbody></table>
            </fieldset>
          </td>
        </tr>

      </tbody></table>
    </fieldset></div>';
    }
    
    echo '    <div class="frame3d">
     <input type="submit" value="Enregistrer"/>
    </div>
  </form>
';
}

function displayEditP1 ()
{
  echo '    <div class="frame3dtitle">
      <h1>Modification d\'une entrée</h1>
      </div>
      <div class="frame3d"><fieldset>
        <legend>Choix du type d\'entrée</legend>
        <table><tbody>
          <tr>
            <th>Clé</th><th>Nom</th>
          </tr>
          <tr>
            <td><a href="">aconit</a></td>
            <td><a href="">ACONIT</a></td>
          </tr>
          <tr>
            <td><a href="">aconit_xmlrpc</a></td>
            <td><a href="">ACONIT (xml-rpc)</a></td>
          </tr>
          <tr>
            <td><a href="">aconitgre</a></td>
            <td><a href="">ACONIT - Base grenoble</a></td>
          </tr>
          <tr>
            <td><a href="">aconitgre_xmlrpc</a></td>
            <td><a href="">ACONIT - Base grenoble (xml-rpc)</a></td>
          </tr>
          <tr>
            <td><a href="">feb</a></td>
            <td><a href="">Fédération des Équipes Bull</a></td>
          </tr>
          <tr>
            <td><a href="">cnam</a></td>
            <td><a href="">CNAM - Musée des Arts et Métiers</a></td>
          </tr>
          <tr>
            <td><a href="">mo5</a></td>
            <td><a href="">MO5</a></td>
          </tr>
          <tr>
            <td><a href="">pstc</a></td>
            <td><a href="">PSTC</a></td>
          </tr>
          <tr>
            <td><a href="">wda</a></td>
            <td><a href="">Winter Development Association</a></td>
          </tr>
          <tr>
            <td><a href="">lecoq</a></td>
            <td><a href="">Musée Lecoq</a></td>
          </tr>
        </tbody></table>
      </fieldset></div>
';
}

function displayDeleteP1 ()
{
  echo '    <div class="frame3dtitle">
      <h1>Suppression d\'une entrée</h1>
    </div>
      <div class="frame3d"><fieldset>
        <legend>Choix du type d\'entrée</legend>
        <table><tbody>
          <tr>
            <th>Clé</th><th>Nom</th><th>Suppression</th>
          </tr>
          <tr>
            <td>aconit</td>
            <td>ACONIT</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>aconit_xmlrpc</td>
            <td>ACONIT (xml-rpc)</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>aconitgre</td>
            <td>ACONIT - Base grenoble</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>aconitgre_xmlrpc</td>
            <td>ACONIT - Base grenoble (xml-rpc)</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>feb</td>
            <td>Fédération des Équipes Bull</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>cnam</td>
            <td>CNAM - Musée des Arts et Métiers</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>mo5</td>
            <td>MO5</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>pstc</td>
            <td>PSTC</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>wda</td>
            <td>Winter Development Association</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
          <tr>
            <td>lecoq</td>
            <td>Musée Lecoq</td>
            <td><button onclick="alert (\'Etes-vous sûr de vouloir supprimer cette entrée ?\');">X</button></td>
          </tr>
        </tbody></table>
      </fieldset></div>
';
}

function displayPopup ($msg, $title, $msgtype)
{
  /*
   * 0 : notify
   * 1 : error
   */

  echo '<div class="frame3dtitle">';

  if ($msgtype == 0)
  {
    echo '    <h1>Confirmation</h1>
';
  }
  elseif ($msgtype == 1)
  {
    echo '    <h1>Erreur</h1>
';
  }
  
  echo '      <h2><i>'.$title.'</i></h2>
    </div>
    <div class="frame3d">
      <p>'.$msg.'</p>
    </div>
';
}

?>