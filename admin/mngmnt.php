<?php
include ('./functions/dom.php');
include ('./functions/content.php');

echo '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">

  <head>
    <title>Gestion des entrées v0.1 beta</title>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="styles/mngmnt.css" />
  </head>
  <body>
    <div id="header">
      <div class="version"><p>Gestion des entrées</p></div>
    </div><!-- Fin du bloc [header] -->
  <div id="content" style="margin-left:auto; margin-right:auto; margin-top:0; margin-bottom:0; width:500px;">
';

$action = isset ($_GET['action']) ? $_GET['action'] : null;

if ($action == 'add')
{
  // Types d'entrées
  $types = array ('ta', 'ws');

  if (isset ($_GET['typeofentry'], $_GET['key'], $_GET['name']))
  {
    displayPopup ('Entrée '.$_GET['key'].' enregistrée avec succès.', 'Enregistrement d\'une entrée', 0);
  }
  elseif (isset ($_GET['typeofentry'], $_GET['key']) && in_array ($_GET['typeofentry'], $types))
  {
    // Page #2 d'ajout - Entrée des informations
    if ($_GET['key'] != null &&
        $_GET['typeofentry'] != null
       )
      displayAddP2 ($_GET['typeofentry'], $_GET['key'], $types, $action);
    else
      displayPopup ('Le champ "Clé d\'index" doit être renseigné.', 'Enregistrement d\'une entrée', 1);
  }
  else
  {
    // Page #1 d'ajout - Choix du type d'entrée
    displayAddP1 ($action);
  }
}
elseif ($action == 'edit')
{
  displayEditP1 ();
}
elseif ($action == 'delete')
{
  displayDeleteP1 ();
}
else
{
  echo '    <div class="frame3dtitle">';
  echo '      <h1>Gestion des entrées</h1>';
  echo '      <h2>Action</h2>';
  echo '    </div>';
  
  echo '    <div class="frame3d">';
  echo '    <ul>';
  echo '      <li><a href="?action=add">Ajouter</a></li>';
  echo '      <li><a href="?action=edit">Editer</a></li>';
  echo '      <li><a href="?action=delete">Supprimer</a></li>';
  echo '    </ul>';
  echo '    </div>';
}

echo '  </div><!-- Fin du bloc [content] -->';

echo '  <div id="footer">';
echo '    <div class="footer_infos"><span>Portail Multi Accès - ACONIT 2010</span></div><div class="footer_logow3c"><img src="pictures/valid_xhtml11.gif" alt="Valide xhtml 1.0"/><img src="pictures/valid_css.gif" alt="Valide CSS"/></div>';
echo '  </div><!-- Fin du bloc [footer] -->';
echo '  </body>';
echo '</html>';
?>