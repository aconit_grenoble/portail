<?php
  if (!isset ($portal))
  {
    /* Inclusions des classes ********************************************************************/
    require_once ('./functions/class/Portal.php');

    /* Initialisation des objets de recherche ****************************************************/
    $portal = new Portal_Main ();
    $portal->init ();
  }

  $keys = $portal->getKeys ();

$content = '<?xml version="1.0" encoding="UTF-8"?>
<entries>
';
foreach ($keys as $key)
{
    $content .= '  <entry key="'.$key.'">
    <infos>'.$portal->getEntryInfos ($key).'</infos>
    <theme>
';
  foreach ($portal->getEntryThemes ($key) as $theme)
  {
    $content .= '      <value>'.$theme.'</value>
';
  }
  $content .= '    </theme>
  </entry>
';
}
$content .= '</entries>
';

// Changement du type de l'en-t�te en XML, puis envoi des donn�es.
header ('Content-type: text/xml');
echo $content;

?>