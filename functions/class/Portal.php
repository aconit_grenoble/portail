<?php
// Inclusion
require_once ('./functions/xmlrpc-3.0.0.beta/lib/xmlrpc.inc');

// Classes
class Portal_Main
{
  /* Attributs */
  private $_portal;
  
  function __CONSTRUCT ()
  {
    $this->_portal['ta'] = new Portal_TextualAnalysis ();
    $this->_portal['xmlrpc'] = new Portal_XMLRPC ();
    // Furture implémentation de SOAP
    // $this->portal['soap'] = new Portal_SOAP ();
  }
  
  public function init ()
  {
    /* Créations des objets et entrées des données *************************************************/

    // Portal Textual Analysis
    $this->_portal['ta']->setPattern ('keywords', '%k%');
    $this->_portal['ta']->setPattern ('references', '%r%');
    $this->_portal['ta']->setPattern ('numbers', '%n%');
    $this->_portal['ta']->setDisplaySeparator (' ');
    $tab = new XmlToArray ('./entries/TextualAnalysis.xml');
    foreach ($tab->getArray () as $k => $v)
    {
      $this->_portal['ta']->setEntry ($k, $v);
    }
    unset ($k, $v);
    
    // Portal XML-RPC
    $this->_portal['xmlrpc']->setPattern ('keywords', '%k%');
    $this->_portal['xmlrpc']->setPattern ('login', '%log%');
    $this->_portal['xmlrpc']->setPattern ('password', '%pwd%');
    $this->_portal['xmlrpc']->setPattern ('references', '%r%');
    $tab = new XmlToArray ('./entries/WebService.xml');
    foreach ($tab->getArray () as $k => $v)
    {
      $this->_portal['xmlrpc']->setEntry ($k, $v);
    }
    unset ($k, $v);

    // Portal SOAP

  }

  /*
   * Accesseur renvoyant toutes les clés
   */
  public function getKeys ()
  {
    $keys = array ();
    foreach ($this->_portal as $portal)
    {
        $keys = array_merge ($keys, $portal->getKeys ());
    }
    sort ($keys);

    return $keys;
  }

  public function getErrorMessages ()
  {
    $error_msgs = array ();
    foreach ($this->_portal as $errors)
    {
      $errors_result = $errors->getErrorMessages ();
      foreach ($errors_result as $error)
      {
        $error_msgs[] = $error;
      }
    }
    
    return $error_msgs;
  }
  
  public function getNumberOfActiveEntries ()
  {
    $total_active_entries = 0;
    foreach ($this->_portal as $active_entries)
    {
      $total_active_entries += $active_entries->getNumberOfActiveEntries ();
    }
    
    return $total_active_entries;
  }
  
  public function getNumberOfEntries ()
  {
    $total_entries = 0;
    foreach ($this->_portal as $entries)
    {
      $total_entries += $entries->getNumberOfEntries ();
    }
    
    return $total_entries;
  }

  /*
   * Getter - Pour obtenir les thèmes associés à l'entrée demandée.
   *
   * Retourne : Vector[String] - Tableau contenant les thèmes d'une entrée.
   */
  public function getEntryThemes ($key)
  {
    $themes = array ();
    
    foreach ($this->_portal as $portal)
    {
      if (!empty ($themes))
        break;

      $themes = $portal->getEntryThemes ($key);
    }
    return $themes;
  }

  /*
   * Getter - Pour obtenir le nom de l'entrée demandée.
   *
   * Retourne : String - Nom de l'entrée.
   */
  public function getEntryName ($key)
  {
    $name = null;
    
    foreach ($this->_portal as $portal)
    {
      if (!empty ($name))
        break;

      $name = $portal->getEntryName ($key);
    }
    return $name;
  }

  /*
   * Getter - Pour obtenir l'état d'activation de l'entrée demandée.
   *
   * Retourne : Boolean - Etat: true = désactivée, false = activée.
   */
  public function getEntryState ($key)
  {
    $state = null;
    foreach ($this->_portal as $portal)
    {
      if (!empty ($state))
        break;

      $state = $portal->getEntryState ($key);
    }
    return $state;
  }

  /*
   * Getter - Pour obtenir la description d'une entrée.
   *
   * Retourne : String - Description de l'entrée.
   */
  public function getEntryInfos ($key)
  {
    $infos = null;
    foreach ($this->_portal as $portal)
    {
      if (!empty ($infos))
        break;

      $infos = $portal->getEntryInfos ($key);
    }
    return $infos;
  }

  public function getEntryWebSite ($key)
  {
    $website = null;
    foreach ($this->_portal as $portal)
    {
      if (!empty ($website))
        break;

      $website = $portal->getEntryWebSite ($key);
    }
    return $website;
  }

  public function search ($keys, $keywords)
  {
    $globalresult = array ();
    
    foreach ($this->_portal as $portal)
    {
      foreach ($portal->search ($keys, $keywords) as $k => $entry)
        $globalresult[$k] = $entry;
    }
    ksort ($globalresult);

    return $globalresult;
  }
}

abstract class Portal
{
  /* Attributs */
  protected $_entries = array ();
  protected $_error_reported;
  protected $_error_msgs = array ();
  protected $_themes = array ('informatique', 'PSTC');
  protected $_patterns = array ('keywords' => '%keywords%', 'login' => '%login%', 'password' => '%password%', 'references' => '%reference%', 'numbers' => '%number%');
  
  /* Méthodes */
  abstract protected function setEntry ($key, $entry);
  abstract protected function buildRequest ($key, $entry, $keywords);
  abstract protected function patternReplacer ($from, $in, $type);
  
  /*
   * Setter - Pour définir l'entité à remplacer par les mots-clés d'une recherche.
   * Le nouveau motif doit être délimité par le caractère % et doit contenir au moins un caractère.
   * Cette méthode ne sert définir des motifs personalisés, des motifs par défaut sont assignés a chaque type.
   *
   * Valeurs par défaut : keywords   = %keywords%
   *                      references = %reference%
   *                      numbers    = %number%
   * 
   * Paramétres :
   * o type    : String - Type du motif à remplacer. Les types sont ceux de l'attribut _patterns :
   *             "keywords" pour les mots-clés.
   *             "references" pour les numéros d'inventaire.
   *             "numbers" pour les numérotations.
   * o pattern : String - Nouveau motif.
   */
  public function setPattern ($type, $pattern)
  {
    try
    {
      if ($pattern != null && $pattern[0] == '%' && $pattern[strlen ($pattern)-1] == '%' && strlen ($pattern) >= 3)
      {
        if (array_key_exists ($type, $this->_patterns))
        {
          $this->_patterns[$type] = $pattern;
        }
        else throw new Exception ('le type "'.$type.'" du motif "'.$pattern.'" n\'est pas pris en charge.');
      }
      else throw new Exception ('le motif "'.$pattern.'" n\'est pas valide.');
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   * Accesseur renvoyant toutes les clés
   */
  public function getKeys ()
  {
    return array_keys ($this->_entries);
  }

  /*
   * Getter - Pour obtenir le nombre de bases enregistrées.
   *
   * Retourne : Integer - Le nombre d'entrées totales enregistrées.
   */
  public function getNumberOfEntries ()
  {
    return count ($this->_entries);
  }

  /*
   * Getter - Pour obtenir le nombre de bases enregistrées non désactivées.
   *
   * Retourne : Integer - Le nombre d'entrées enregistrées marquées comme actives.
   */
  public function getNumberOfActiveEntries ()
  {
    $i = 0;
    foreach ($this->_entries as $v)
      if (!$v['disabled'])
        $i ++;

    return $i;
  }

  /*
   * Getter - Pour obtenir les thèmes associés à l'entrée demandée.
   *
   * Retourne : Vector[String] - Tableau contenant les thèmes d'une entrée.
   */
  public function getEntryThemes ($key)
  {
    if (array_key_exists ($key, $this->_entries))
    {
      return $this->_entries[$key]['themes'];
    }
    else return null;
  }

  /*
   * Getter - Pour obtenir le nom de l'entrée demandée.
   *
   * Retourne : String - Nom de l'entrée.
   */
  public function getEntryName ($key)
  {
    if (array_key_exists ($key, $this->_entries))
    {
      return $this->_entries[$key]['name'];
    }
    else return null;
  }

  /*
   * Getter - Pour obtenir l'état d'activation de l'entrée demandée.
   *
   * Retourne : Boolean - Etat, true = désactivée, false = activée.
   */
  public function getEntryState ($key)
  {
    if (array_key_exists ($key, $this->_entries))
    {
      return $this->_entries[$key]['disabled'];
    }
    else return null;
  }

  /*
   * Getter - Pour obtenir la description d'une entrée.
   *
   * Retourne : String - Description de l'entrée.
   */
  public function getEntryInfos ($key)
  {
    if (array_key_exists ($key, $this->_entries))
    {
      return $this->_entries[$key]['infos'];
    }
    else return null;
  }

  /*
   * Getter - Pour obtenir le site internet d'une association
   *
   * Paramétres :
   * o key : String - Clé d'une entrée dont on veut le site web.
   *
   * Retourne : String - Le site web de l'entrée voulue.
   */
  public function getEntryWebSite ($key)
  {
    if (array_key_exists ($key, $this->_entries))
    {
      return $this->_entries[$key]['website'];
    }
    else return null;
  }

  /*
   * Getter - Renvoi tous les messages d'erreurs levés par une exception.
   *
   * Retourne : Vector<String> - Les messages d'erreurs.
   */
  public function getErrorMessages ()
  {
    return $this->_error_msgs;
  }

  /*
   * Méthode publique permettant d'effectuer une recherche sur les bases actives à partir d'un ou plusieurs mots-clés.
   *
   * Paramètres :
   * o $keys : Vector[String] - Liste des clés des entrées à rechercher.
   *           (ex : aconit, aconitgre, etc)
   * o $keywords : String - Mots-clés d'une recherche.
   *
   * Retourne :  Vector[Vector] - Tableau de tous les résultats renvoyés par la fonction buildRequest.
   */
  public function search ($keys, $keywords)
  {
    try
    {
      $results = array ();
      /* Si il y a un mot-clé et que 'all' se trouve dans la liste des bases choisies faire une recherche sur toutes les bases */
      if ($keywords !== null && in_array ('all', $keys))
      {
        foreach ($this->_entries as $key => $entry)
        {
          if (!$entry['disabled'])
          {
            $results[$key] = $this->buildRequest ($key, $entry, $keywords);
          }
        }
        return $results;
      }
      /* Si il y a seulement un mot-clé, faire une recherche sur les bases sélectionnées */
      elseif ($keywords !== null)
      {
        foreach ($this->_entries as $key => $entry)
        {
          if (in_array ($key, $keys) && !$entry['disabled'])
          {
            $results[$key] = $this->buildRequest ($key, $entry, $keywords);
          }
        }

        return $results;
      }
      else throw new Exception ('les paramètres "keys" et "keywords" doivent être renseignés.');
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   * Méthode protégée - Empile le message d'erreur dans la liste des messages d'erreur et passe
   *                  l'indicateur d'erreur à vrai pour indiquer qu'une erreur est survenue.
   *
   * Paramètres :
   * e : Exception - Exception dont ont veut récupérer le message d'erreur.
   */
  protected function exceptionMsg (&$e)
  {
    $this->_error_msgs[] = 'Erreur Portal_TextualAnalysis : '.$e->GetMessage ();
    $this->_error_reported = true;
  }

  /*
   * Méthode protégée - Fait des appels sur des filtres afin de vérifier qu'un résultat est correct
   */
  protected function applyFilters (&$result, $keywords)
  {
    /*
     * Filtre supprimant les résultats qui ni comportent pas tous les mots-clés d'une recherche
     */
    {
      // Eclatement des mots-clés
      preg_match_all ('#[\s]*([\S]*)#i', $keywords, $out);
      // Récupération des mots-clés sous forme de tableau
      $out = $out[1];

      if ($result['number'] != 0)
      {
        // Pour chaque description courte d'objet, vérification de la présence de tout les mots-clés, sinon suppression de l'entrée
        $i = 0;
        foreach ($result['datas'] as $k => $v)
        {
          foreach ($out as $keyword)
          {
            if ($keyword != null)
            {
              if (strpos (mb_strtolower ((string)$result['datas'][$k]['name'], 'UTF-8'), mb_strtolower ($keyword, 'UTF-8')) === false)
              {
                unset ($result['datas'][$k], $result['links'][$k]);
              }
            }
          }
          
          // Réorganisation des éléments en incrémentation directe
          if (isset ($result['datas'][$k]))
          {
            $resultmp['datas'][$i] = $result['datas'][$k];
            $resultmp['links'][$i] = $result['links'][$k];
            $i ++;
          }
        }
        
        // Remplacement des anciennes valeurs pas les nouvelles
        $result['number'] = count ($result['datas']);
        $result['datas'] = $resultmp['datas'];
        $result['links'] = $resultmp['links'];
      }
    }
  }
}

abstract class Portal_WebServices extends Portal
{
  /* Attributs */
  
  /* Méthodes */
  abstract protected function init ($entry);
  abstract protected function createMessage ($methodname, $msgvalue);
  abstract protected function request ($client, $message, $timeout=0, $transport='http');
  abstract protected function convert ($value);
}

final class Portal_XMLRPC extends Portal_WebServices
{
	/* Attributs */


	/* Méthodes */
	
	/*
	 * Constructeur de la classe
	 * Peut intialiser directement une entrée si celle-ci est envoyé en paramètre.
	 *
	 * Paramètres optionels :
   * $entry_name : nom de l'index (ex: aconit)
   * $entry      : tableau d'informations de recherche sur une association
	 */
	function __CONSTRUCT ($entry_name = null, $entry = null)
	{
    if ($entry_name !== null && $entry !== null)
      $this->setEntry ($entry_name, $entry);
	}

  /*
   *
   */
  public function setEntry ($key, $entry)
  {
    try
    {
      // Vérifie que l'index n'est pas nul
      if (isset ($key) && is_string ($key) && $key !== '')
      {
        // Et vérifie que les champs primaires sont correctement définis
        if (isset ($entry) && is_array ($entry))
        {
          $this->_entries[$key] = $entry;
          
          ksort ($this->_entries);
        }
        
      }
      else throw new Exception ('problème sur un index, un index doit être spécifié sous forme de chaîne de caractère non vide');
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   *
   */
  protected function buildRequest ($key, $entry, $keywords)
  {
    // Initialisation du client
    $client = $this->init ($entry);
    $client->setDebug (0);
    // Encodage UTF-8
    $client->request_charset_encoding = 'UTF-8';

    // Création du message de la requête
    $vals = array ();
    foreach ($entry['parameters']['values'] as $values)
    {
      // Remplacement des patterns
      if ($entry['server']['login'] !== null)
        $values['value'] = $this->patternReplacer ($entry['server']['login'], $values['value'], 'login');
      if ($entry['server']['password'] !== null)
        $values['value'] = $this->patternReplacer ($entry['server']['password'], $values['value'], 'password');
      $values['value'] = $this->patternReplacer ($keywords, $values['value'], 'keywords');

      // Ajout des valeurs XML-RPC
      $vals[] = new xmlrpcval ($values['value'], $values['type']);
    }
    unset ($values);
    $message = $this->createMessage ($entry['parameters']['methodname'], $vals);

    // Envoi et réception de la requête
    $response = $this->request ($client, $message);

    // Traitement du résultat de la requête
    $result = $this->dataProcessing_XMLRPC ($response, array ('domain' => $entry['domain'], 'link' => $entry['link']), $entry['parameters']);
    
    $this->applyFilters ($result, $keywords);

    return array ('result_number' => $result['number'],
                  'datas' => $result['datas'],
                  'links' => $result['links']
                 );
  }

  /*
   *
   */
  protected function patternReplacer ($from, $in, $type)
  {
    try
    {
      if (array_key_exists ($type, $this->_patterns))
      {
        return str_replace ($this->_patterns[$type], $from, $in);
      }
      else throw new Exception ('le type "'.$type.' n\'est pas un type valide');
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   *
   */
  protected function init ($entry)
  {
    return new xmlrpc_client ($entry['server']['path'], $entry['domain'], $entry['server']['port'], $entry['server']['transport']); 
  }

  /*
   *
   */
  protected function createMessage ($methodname, $msgvalue)
  {
    return new xmlrpcmsg ($methodname, $msgvalue);
  }

  /*
   *
   */
  protected function request ($client, $message, $timeout=0, $transport='http')
  {
    return $client->send ($message, $timeout, $transport);
  }

  /*
   *
   */
  private function dataProcessing_XMLRPC ($response, $urlelements, $parameters)
  {
    try
    {
      if (!$response)
      {
        throw new Exception ('le serveur XML-RPC ne peut pas être contacté');
      }
      elseif ($response->faultCode ())
      {
        throw new Exception ('le serveur XML-RPC a généré l\'erreur "'.$response->faultCode ().'" : "'.$response->faultString ().'"');
      }
      else
      {
        $response_serialized = str_replace (array ('&lt;', '&gt;'), array ('<', '>'), $response->serialize ());
        $xml = new SimpleXMLElement ($response_serialized);
        
        if (count($xml) > 0)
        {
          $datas = array ();
          $i = 0;
          foreach ($xml->params->param as $param)
          {
            foreach ($param->value->struct->member as $param_member)
            {
              if (in_array ($param_member->name, $parameters['resultsname']))
              {
                /* si la réponse ne comprend ni l'élément <value><array>..</array></value>
                 * ni l'élément <value><data>...</data></value> alors c'est qu'elle n'a donné
                 * aucun résultat.
                 * Il n'y a rien à traiter et un tableau vide est renvoyé.
                 */
                if (isset ($param_member->value->array) || isset ($param_member->value->data))
                {
                  $data[$i] = array ();
                  foreach ($param_member->value->array->data->value as $value)
                  {
                    foreach ($value->struct->member as $value_member)
                    {
                      foreach ($parameters['elements'] as $k => $v)
                      {
                        if ($value_member->name == $v)
                        {
                          /* (?)
                           * Traitement sur les chaînes de caractères
                           */
                          if (isset ($value_member->value->string))
                          {
                            if ($k == 'reference')
                              $datas[$i][$k] = (string)$value_member->value->string;
                            elseif ($k == 'type')
                              $datas[$i][$k] = (string)$value_member->value->string;
                            elseif ($k == 'name')
                              $datas[$i][$k] = (string)$value_member->value->string;
                            elseif ($k == 'link')
                              $links[$i] = $this->makeLinkStructure_XMLRPC($urlelements, (string)$value_member->value->string);
                          }
                        }
                      }
                    }
                    $i ++;
                  }
                }
                else
                {
                  return array ('number' => 0, 'datas' => null, 'links' => null);
                }
              }
              else throw new Exception ('le format du résultat n\'est pas correct, il devrait être '.$parameters['searchname']);
            }
          }
          unset ($xml, $v, $value_member, $value, $param_member, $param);
        }
        else throw new Exception ('le document est invalide');
      }
      return array ('number' => $i, 'datas' => $datas, 'links' => $links);
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   *
   */
  private function makeLinkStructure_XMLRPC ($urlelements, $reference)
  {
    $link = array ('url' => 'http://'.$urlelements['domain'].$urlelements['link']['path']);
    // Si le champ "get" n'est pas nul
    if ($urlelements['link']['get'] !== null)
    {
      $link['url'] .= '?';
      foreach ($urlelements['link']['get'] as $kg => $vg)
      {
        $link['url'] .= $kg.'='.$vg.'&';
      }
      unset ($vg);
      $link['url'] = substr ($link['url'], 0, -1);
      $link['url'] = str_replace ($this->_patterns['references'], $reference, $link['url']);
    }
    
    $link['postdatas'] = null;

    return $link;
  }

  /*
   * /!\ Implémentation partielle, mais fonctionnelle, inutilisée dans la version 0.6 /!\
   * Méthode privée - Analyse une valeur d'une réponse XML-RPC et la retourne dans un format compréhensible par PHP.
   *
   * Paramètres :
   * o value : xmlrpcval - Valeur XML-RPC à décoder.
   *
   * Retourne : Mixed - Type PHP exploitable.
   */
  protected function convert ($value)
  {
    switch ($value->kindOf ())
    {
      case 'array':
      {
        $datas = array ();
        echo 'type : array<br />';
        for ($i = 0; $i < $value->arraySize (); $i++)
        {
          $datas[] = $value->arrayMem ($i);
        }
        break;
      }

      case 'scalar':
      {
        echo 'type : scalar<br />';
        $datas = $value->scalarVal ();
        break;
      }
      
      case 'struct':
      {
        $datas = array ();
        echo 'type : struct<br />';
        $value->structReset ();
        while (list ($key, $valuetmp) = $value->structEach())
        {
          echo '* '.$key.' of '.$valuetmp->kindOf ().'<br />';
          $datas[$key] = $this->xmlrpcToPhp ($valuetmp);
        }
        break;
      }
      
      case 'undef':
      {
        echo 'type : undef<br />';
        break;
      }
      
      default:
        echo 'ne correspond à aucun type connu<br />';
    }
    
    return $datas;
  }

}

final class Portal_TextualAnalysis extends Portal
{
  /* Attributs */
  private $_display_separator = ' - ';
  
  /* Méthodes */
  
  /*
   * Constructeur de la classe - Peut initialiser directement une entrée si celle-ci est envoyée en paramètre.
   *
   * Paramètres optionels :
   * o $entry_name : String - Nom de l'index (ex: aconit).
   * o $entry      : Vector - Tableau d'informations de recherche sur une association (cf. méthode setEntry).
   */
  function __CONSTRUCT ($entry_name = null, $entry = null)
  {
    $this->_error_reported = false;
    if ($entry_name !== null && $entry !== null)
      $this->setEntry ($entry_name, $entry);
  }
  
  /*
   * Méthode publique - Permet de définir les associations dont on veut consutler les données.
   *
   * Paramétres :
   * o key   : Clé qui indexe l'entrée (ex: aconit).
   * o entry : Tableau d'informations d'une entrée d'association, contient :
   *   |-> name     : String - Nom complet de l'association.
   *   |-> infos    : String - Description rapide de l'entrée.
   *   |-> theme    : String|Vector - Thème(s) à prendre en compte lors d'une recherche (Informatique, PSTC).
   *   |-> website  : String - Adresse du site web de l'association (ex : http://www.aconit.fr/index.php).
   *   |-> domain   : String - Nom du domaine (ex : db.aconit.org).
   *   |              Utilisé lors de la construction des liens, l'appel des pages, etc...
   *   |-> search   : Vector - Règles de recherches dans les pages web, contient :
   *   |   |-> path    : String - chemin absolu de la page dans laquelle effectuer une recherche dans le domaine (ex : /dbaconit/rechercher.php).
   *   |   |-> get     : Vector[String|Integer] - Tableau d'entrée des valeurs à passer en GET (ex : param => valeur).
   *   |   |-> post    : Vector[String|Integer] - Tableau d'entrée des valeurs à passer en POST (ex : param => valeur).
   *   |   |-> filter  : Vector[String]|Vector[vector[String]] - Possibilité de déclarer un ou plusieurs filtre(s).
   *   |   |   |         Ils sont appelés succéssivement tant que le résultat est nul.
   *   |   |   |         Chaque filtre comprend les champs :
   *   |   |   |-> regex    : String - Expression rationnelle qui permet l'extraction du contenu de la recherche.
   *   |   |   |              Si des balises HTML (<b>, <i>, <strong>, etc) sont présentes dans une partie capturante,
   *   |   |   |              elles sont autommatiquement suprimmées lors du traitement.
   *   |   |   |-> template : String - Motif qui indique l'ordre de traitement des parties capturées par l'expression rationelle.
   *   |   |   |              (link/ref#/type#/name#), les paramètres avec # peuvent être décomposés et numérotés.
   *   |   |   |              (ex : n1/r/l/n3/t2,n2/t1 donnera r/t1-t2/n1-n2-n3 lors du traitement, il y a concaténation des paramètre de même type).
   *   |   |   |              Le template 'l' permet d'indiquer un lien à extraire pour la consultation des fiches.
   *   |   |   +-> drop     : Null|String|Vector[String] - Chaîne(s) de caractères à supprimer. Permet un traitement complémentaire à l'expression rationnelle.
   *   |   |
   *   |   +-> repeat : Null|Vector - à utiliser dans le cas où une recherche à besoin de s'effectuer sur plusieurs pages, contient :
   *   |       |-> first    : Integer - Premier élément de la première page (généralement 0 ou 1).
   *   |       |-> per_page : Integer - Nombre total de pages à rechercher.
   *   |       +-> regex    : String - Expression rationelle d'extraction du nombre de page (implique le présence de cet élément dans une page web).
   *   |
   *   |-> link     : Vector - Structure du lien vers la page de la fiche description, un lien peut soit être construit ici,
   *   |   |          soit être extrait grâce à l'expression rationelle de 'search' et le template 'l', contient :
   *   |   |-> path    : Null|String - Chemin du lien, la construction se fait ainsi http:// + nom du domaine + path + info get.
   *   |   |-> get     : Null|Vector[String|Integer] - Tableau d'entrée des valeurs à passer en GET.
   *   |   |-> post    : Null|Vector[String|Integer] - Tableau d'entrée des valeurs à passer en POST.
   *   |   +-> filter  : Null|String - Permet de donner un règle d'extraction par expression rationnelle sur un lien extrait avec un template 'l'.
   *   |
   *   +-> disabled : Bool - définit si la table doit être exclue des recherches.
   */
  public function setEntry ($key, $entry)
  {
    try
    {
      // Vérifie que l'index n'est pas nul
      if (isset ($key) && is_string ($key) && $key !== '')
      {
        // Et vérifie que les champs primaires sont correctement définis
        if (isset ($entry) && is_array ($entry))
        {
          if (!isset ($entry['name']))
            throw new Exception ('le champ "name" dans "'.$key.'" n\'est pas défini, aucun nom d\'association spécifié');
          elseif (!is_string ($entry['name']) || $entry['name'] === '')
            throw new Exception ('le champ "name" dans "'.$key.'" doit être une chaîne de caractère non vide');

          if (!isset ($entry['themes']))
            throw new Exception ('le champ "themes" dans "'.$key.'" n\'est pas défini, aucun thème de recherche spécifié');
          elseif (!is_array ($entry['themes']) && !is_string ($entry['themes']))
            throw new Exception ('le champ "themes" dans "'.$key.'" doit être un tableau ou une chaîne de caractère');

          if (!isset ($entry['website']))
            throw new Exception ('le champ "website" dans "'.$key.'" n\'est pas défini, aucune adresse web spécifiée');
          elseif (!is_string ($entry['website']) || $entry['website'] === '')
            throw new Exception ('le champ "website" dans "'.$key.'" doit être une chaîne de caractère non vide');

          if (!isset ($entry['domain']))
            throw new Exception ('le champ "domain" dans "'.$key.'" n\'est pas défini, aucun nom de domaine ("www.monsite.com") spécifié');
          elseif (!is_string ($entry['domain']) || $entry['domain'] === '')
            throw new Exception ('le champ "domain" dans "'.$key.'" doit être une chaîne de caractère non vide');

          if (!isset ($entry['search']))
            throw new Exception ('le champ "search" dans "'.$key.'" n\'est pas défini, aucun paramètre de recherche spécifié');
          elseif (!is_array ($entry['search']))
            throw new Exception ('le champ "search" dans "'.$key.'" doit être un tableau');
          else
          {
            // Vérification de la validité des entrées du champ search
            if (!isset ($entry['search']['path']))
              throw new Exception ('le champ "search/path" dans "'.$key.'" est nul ou non défini');
            elseif (!is_string ($entry['search']['path']) || $entry['search']['path'] === '')
              throw new Exception ('le champ "search/path" dans "'.$key.'" doit être une chaîne de caractère non vide');

            if (isset ($entry['search']['get']) && !is_array ($entry['search']['get']))
              throw new Exception ('le champ "search/get" dans "'.$key.'" doit être un tableau');

            if (isset ($entry['search']['post']) && !is_array ($entry['search']['post']))
              throw new Exception ('le champ "search/post" dans "'.$key.'" doit être un tableau');

            if (!isset ($entry['search']['get']) && !isset ($entry['search']['post']))
              throw new Exception ('les champs "search/get" et "search/post" dans "'.$key.'" ne peuvent pas être vides tous les deux');
              
            if (!isset ($entry['search']['filter']))
              throw new Exception ('le champ "search/filter" dans "'.$key.'" n\'est pas défini');
            elseif (!is_array ($entry['search']['filter']))
              throw new Exception ('le champ "search/filter" dans "'.$key.'" doit être un tableau');
            elseif (is_array ($entry['search']['filter']))
            {
              if (!isset ($entry['search']['filter']['regex']) && !is_array ($entry['search']['filter']))
                throw new Exception ('le champ "search/filter/regex" dans "'.$key.'" est nul ou non défini');
              if (!isset ($entry['search']['filter']['template']) && !is_array ($entry['search']['filter']))
                throw new Exception ('le champ "search/filter/template" dans "'.$key.'" est nul ou non défini');
            }

            if (isset ($entry['search']['repeat']) && !is_array ($entry['search']['repeat']))
              throw new Exception ('le champ "search/repeat" dans "'.$key.'" n\'est pas défini');
          }

          if (!isset ($entry['link']))
            throw new Exception ('le champ "link" dans "'.$key.'" n\'est pas défini, aucun paramètre de hyperlien spécifié');
          elseif (!is_array ($entry['link']))
            throw new Exception ('le champ "link" dans "'.$key.'" doit être un tableau');
          else
          {
            // Vérification de la validité des entrées du champ link
            if (!isset ($entry['link']['path']))
              throw new Exception ('le champ "path" de "link" dans "'.$key.'" est nul ou non défini');
            elseif (!is_string ($entry['link']['path']) || $entry['link']['path'] === '')
              throw new Exception ('le champ "link/path" dans "'.$key.'" doit être une chaîne de caractère non vide');

            if (isset ($entry['link']['get']) && !is_array ($entry['link']['get']))
              throw new Exception ('le champ "link/get" dans "'.$key.'" doit être un tableau');

            if (isset ($entry['link']['post']) && !is_array ($entry['link']['post']))
              throw new Exception ('le champ "link/post" dans "'.$key.'" doit être un tableau');
            
            /*if (!isset ($entry['link']['filter']))
              throw new Exception ('le champ "link/filter" dans "'.$key.'" n\'est pas défini');
            elseif (!is_array ($entry['link']['filter']))
              throw new Exception ('le champ "link/filter" dans "'.$key.'" doit être un tableau');
            elseif (is_array ($entry['link']['filter']))
            {
              if (!isset ($entry['link']['filter']['regex']) && !is_array ($entry['link']['filter']))
                throw new Exception ('le champ "link/filter/regex" dans "'.$key.'" est nul ou non défini');
              if (!isset ($entry['link']['filter']['template']) && !is_array ($entry['link']['filter']))
                throw new Exception ('le champ "link/filter/template" dans "'.$key.'" est nul ou non défini');
            }*/
          }

          if (!isset ($entry['disabled']))
            throw new Exception ('le champ "disabled" dans "'.$key.'" n\'est pas défini, aucune information sur l\'état d\'activation de l\'entrée ne peut être utilisée');
          elseif (!is_bool ($entry['disabled']))
            throw new Exception ('le champ "disabled" dans "'.$key.'" doit être un booléen');
            
          $this->_entries[$key] = $entry;
        
          ksort ($this->_entries);
        }
        else throw new Exception ('paramètres de recherche incorrects ou manquants, "'.$key.'" doit utiliser un tableau');
      }
      else throw new Exception ('problème sur un index, un index doit être spécifié sous forme de chaîne de caractère non vide');
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }
  
  /*
   * Setter - Pour définir le séparateur qui sera utilisé pour concaténer les informations sur les machines.
   *
   * Valeur par défaut : " - "
   *
   * Paramétres :
   * o display_separator : String - Nouveau séparateur.
   */
  public function setDisplaySeparator ($display_separator)
  {
    $this->_display_separator = $display_separator;
  }



  /*
   * Méthode privée - Construit les recherches sur une entrée
   *
   * Paramètres :
   * o key      : String - Clé de l'entrée dont on veut contruire une recherche.
   * o entry    : Vector - Informations d'une entrée (cf. fonction setEntry).
   * o keywords : String - Mots-clés de la recherche.
   *
   * Retourne :  Vector - Qui contient :
   *             |-> result_number : Integer - Nombre de résultats trouvés.
   *             |-> datas         : Vector[Vector[String]]
   *             |                          |-> reference : String - Numéro d'inventaire du résultat.
   *             |                          |-> type      : String - Type auquel appartient le résultat (ordinateur, ouvrage, etc).
   *             |                          +-> name      : String - Nom ou modèle du résultat.
   *             |
   *             +-> links         : Vector[String] - Liens vers les fiches à consulter.
   */
  protected function buildRequest ($key, $entry, $keywords)
  {
    $link1 = $this->patternReplacer ($entry['search'], $keywords, 'keywords');

    if (isset ($entry['search']['repeat']))
    {
      $link2 = $this->patternReplacer ($link1, $entry['search']['repeat']['first'], 'numbers');
      $html = $this->receiveHtml ($this->makeRequest ($entry, $link2));
      
      if (preg_match ($entry['search']['repeat']['regex'], $html, $total) != 0)
      {
        $total = intval ($total[1]);
        for ($i=($entry['search']['repeat']['first']+$entry['search']['repeat']['per_page']); $i<($total+$entry['search']['repeat']['first']); $i+=$entry['search']['repeat']['per_page'])
        {
          $link2 = $this->patternReplacer ($link1, $i, 'numbers');
          $html .= $this->receiveHtml ($this->makeRequest ($entry, $link2));
        }
      }
    }
    else
    {
      $html = $this->receiveHtml ($this->makeRequest ($entry, $link1));
    }

    $result = $this->dataProcessing_TA ($this->extractContent ($html, $entry['search']['filter']), array ('domain' => $entry['domain'], 'link' => $entry['link']), $entry['search']['filter'], $keywords);

    $this->applyFilters ($result, $keywords);
    
    return array ('result_number' => $result['number'],
                  'datas' => $result['datas'],
                  'links' => $result['links']
                 );
  }

  /*
   * Méthode privée - Remplace pour un type donné (keywords, references, etc) les %xxxx% correspondants à ce type par la valeur souhaitée.
   *
   * Paramètres :
   * o from : Vector - Paramètres d'hyperliens HTML.
   *          |-> get  : Vector[String] - Paramètres GET (ex : param => valeur).
   *          +-> post : Vector[String] - Paramètres POST (ex : param => valeur).
   * o by   : String - Valeur de remplacement.
   * o type : String - Types définis dans l'attribut _patterns.
   *
   * Retourne : Vector - Lien final correspondant à la requête.
   *            |-> get  : Vector[String] - Paramètres GET (ex : param => valeur).
   *            +-> post : Vector[String] - Paramètres POST (ex : param => valeur).
   */
  protected function patternReplacer ($from, $in, $type)
  {
    try
    {
      $get = null;
      $post = null;
      if ($this->_patterns[$type] !== null)
      {
        // Remplacement des motifs dans GET par leur valeur.
        if (isset ($from['get']))
        {
          $get = $from['get'];
          if ($get !== null)
          {
            foreach ($get as &$v)
            {
              $v = str_replace ($this->_patterns[$type], $in, $v);
            }
            unset ($v);
          }
        }

        // Remplacement des motifs dans POST par leur valeur.
        if (isset ($from['post']))
        {
          $post = $from['post'];
          if ($post !== null)
          {
            foreach ($post as &$v)
            {
              $v = str_replace ($this->_patterns[$type], $in, $v);
            }
            unset ($v);
          }
        }
      }
      else throw new Exception ('aucun motif de remplacement n\'a été défini.');
      
      return array ('get' => $get, 'post' => $post);
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   * Méthode privée - Construit un lien à partir des données d'une entrée.
   *
   * Paramètres :
   * o entry   : Vector - Informations d'une entrée (cf. fonction setEntry).
   * o link    : Vector - Lien à construire.
   *             |-> get  : Vector[String] - Paramètres GET (ex : param => valeur).
   *             +-> post : Vector[String] - Paramètres POST (ex : param => valeur).
   *
   * Retourne : Vector - Lien formaté, pret pour être exploité.
   *            |-> get  : String - Données POST (ex : http://www.site.com/index.html?u1=v1&u2=v2)
   *            +-> post : String - Données POST (ex : x1=y1&x2=y2&x3=y3)
   */
  private function makeRequest (&$entry, &$link)
  {
    // Construction du lien GET.
    $link_get = 'http://'.$entry['domain'].$entry['search']['path'];

    if ($link['get'] !== null)
    {
      $link_get .= '?';

      foreach ($link['get'] as $k => $v)
      {
        $link_get .= $k.'='.$v.'&';
      }
      $link_get = substr ($link_get, 0, -1);

      unset ($v);
    }
    
    // Construction du lien POST.
    $link_post = '';

    if ($link['post'] !== null)
    {
      foreach ($link['post'] as $k1 => $v1)
      {
        if (is_array ($v1))
        {
          foreach ($v1 as $v2)
          {
            $link_post .= $k1.'[]='.$v2.'&';
          }
          unset ($v2);
        }
        else
        {
          $link_post .= $k1.'='.$v1.'&';
        }
      }
      $link_post = substr ($link_post, 0, -1);
      unset ($v1);
    }

    return array ('get' => $link_get, 'post' => $link_post);
  }

  /*
   * Méthode privée - Effectue une requête serveur afin de télécharger le contenu d'une page web
   *
   * Paramètres :
   * o link : Vector[String] - Lien préformaté avec les champs GET et POST (cf. fonction makeRequest)
   *
   * Retourne : String - Contenu d'une page html.
   */
  private function receiveHtml ($link)
  {
    // Utilisation de cURL pour récupérer le contenu d'une page HTML
    $request = curl_init ($link['get']);
    curl_setopt($request, CURLOPT_HEADER, false);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    if ($link['post'] === null)
    {
      curl_setopt($request, CURLOPT_POST, false);
    }
    else
    {
      curl_setopt($request, CURLOPT_POST, true);
      curl_setopt($request, CURLOPT_POSTFIELDS, $link['post']);
    }
    $html_content = curl_exec($request);
    curl_close($request);

    // Conversion vers UTF-8 si ISO-8859-1
    if(!(mb_detect_encoding($html_content) == 'UTF-8' && mb_check_encoding($html_content, 'UTF-8')))
    {
      $html_content = utf8_encode($html_content);
    }

    return $html_content;
  }

  /*
   * Méthode privée - Extrait le contenu d'une page à partir d'une expression rationelle et le revoit avec son template associé.
   *
   * Paramètres :
   * o html_content : String - Contenu d'une page HTML (cf. fonction receiveHtml)
   * o filter       : Vector - Filtre (cf. fonction setEntry, commentaire entry/search/filter/)
   *
   * Retourne : Vector - Résultat de l'extraction des informations
   *            |-> match    : Vector[Vector[String]] - Résultat de l'extraction, une case par résultat, chaque case contenant les captures de l'expression rationnelle.
   *            |              ex : array ([0] => array ([0] => type0, [1] => type1, [2] => type2, ...),
   *            |                          [1] => array ([0] => refe0, [1] => refe1, [2] => refe2, ...),
   *            |                          [2] => array ([0] => name0, [1] => name1, [2] => name2, ...)
   *            |                         )
   *            |-> template : String - Template défini dans l'entrée (ex : (r/n0/t/n1)), il n'est que transmis.
   *            +-> index    : Integer - Index du tableau si présent dans entry/search/filter//
   */
  private function extractContent ($html_content, &$filter)
  {
    try
    {  
      $result = array ('match' => null, 'template' => null, 'index' => null);
      if ($filter != null) // si différent de 0, '' ou null
      {
        // Lancement des requêtes
        if (!isset ($filter['regex']))
        {
          foreach ($filter as $k => $v)
          {
            // On ne garde que les parties capturantes
            $match_nb = preg_match_all ($v['regex'], $html_content, $match);
            if ($match_nb > 0)
            {
              array_shift ($match);
              $result['match'] = $match;
              $result['template'] = $v['template'];
              $result['index'] = $k;
              break;
            }
            elseif ($match_nb === false) throw new Exception ('erreur lors du traitement de l\'expression régulière.');
          }
          unset ($v);
        }
        else
        {
          if (preg_match_all ($filter['regex'], $html_content, $match))
          {
            array_shift ($match);
            $result['match'] = $match;
            $result['template'] = $filter['template'];
          }
        }
      }
      else throw new Exception ('aucune règle d\'extraction n\'a été définie.');

      return $result;
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   * Méthode privée - Formatte les données extraites pour les rendre exploitables
   *
   * Paramètres :
   * o datas    : Vector - Résultat de l'extraction des informations (cf. fonction receiveHtml)
   * o entry    : Vector - Informations d'une entrée (cf. fonction setEntry).
   * o keywords : String - Mots-clés de la recherche.
   *
   * Retourne : Vector - Données totalement traitées
   *            |-> number : Interger - Nombre d'objets trouvés.
   *            |-> datas  : Vector[Vector[String]] - Descriptions des objets correctement formatés.
   *            |                   |-> reference : String - Numéro d'inventaire d'un objet.
   *            |                   |-> type      : String - Type d'objet (machine, logiciel, etc).
   *            |                   +-> name      : String - Nom de l'objet.
   *            |            ex : array ([0] => Array ([reference] => ACONIT XXXXX,
   *            |                                      [type]      => Machine
   *            |                                      [name]      => Unité centrale Super Ordi
   *            |                                     )
   *            |                        [1] => ...
   *            |                       )
   *            +-> links  : Vector[Vector[String]] - Lien vers les pages des sites
   *                                |-> url       : String - Données GET formatées.
   *                                +-> postdatas : String - Données POST formatées.
   *                         ex : array ([0] => Array ([url]       => http://www.site.com/index.html?u1=v1&u2=v2
   *                                                   [postdatas] => x1=y1&x2=y2&x3=y3
   *                                                  )
   *                                     [1] => ...
   *                                    )
   */
  private function dataProcessing_TA ($datas, $urlelements, $filters, $keywords)
  {
    try
    {
      if ($datas['match'] !== null && $datas['template'] !== null)
      {
        $fields = explode ('/', $datas['template']);
        $nb_machines = count ($datas['match'][0]);
        $pos = 0;
        $datasout = array ();
        $linksout = array ();

        // Affectation des positions de chaque élément
        foreach ($fields as $field)
        {
          if ((($field[0] == 'r' || $field[0] == 't' || $field[0] == 'n') && (is_numeric (substr ($field, 1)) || strlen ($field) == 1)) || ($field == 'l'))
          {
            if ($field == 'l')
            {
                $template['l'] = $pos;
            }
            elseif ($field[0] == 'r')
            {
              $template['r'][intval (substr ($field, 1))] = $pos;
            }
            elseif ($field[0] == 't')
            {
              $template['t'][intval (substr ($field, 1))] = $pos;
            }
            else // $field[0] == 'n'
            {
              $template['n'][intval (substr ($field, 1))] = $pos;
            }
            $pos ++;
          }
          else throw new Exception ('Erreur Multibase : le template "'.$datas['template'].'" n\'est pas correct.');
        }
        
        // Les données sont ré-arrangées par clés
        foreach ($template as &$v)
        {
          if (is_array ($v))
          {
            ksort ($v);
          }
        }
        unset ($v);

        for ($i=0; $i<$nb_machines; $i++)
        {
          /* Initialisation du tableau de sortie */
          $datasout[$i] = array ('reference' => '', 'type' => '', 'name' => '');

          /* Ré-affection et ajout des valeurs dans l'ordre dans le tableau de sortie  */
          if (isset ($template['r']))
          {
            foreach ($template['r'] as $vr)
            {
              $datasout[$i]['reference'] .= $datas['match'][$vr][$i].$this->_display_separator;
            }
            unset ($vr);
          }

          if (isset ($template['t']))
          {
            foreach ($template['t'] as $vt)
            {
              $datasout[$i]['type'] .= $datas['match'][$vt][$i].$this->_display_separator;
            }
            unset ($vt);
          }

          if (isset ($template['n']))
          {
            foreach ($template['n'] as $vn)
            {
              $datasout[$i]['name'] .= $datas['match'][$vn][$i].$this->_display_separator;
              // Suppression des balises SGML
              $datasout[$i]['name'] = strip_tags ($datasout[$i]['name']);
            }
            unset ($vn);
          }

          foreach ($datasout[$i] as $k => &$v_data)
          {
            // Suppression du séparateur de concaténation en trop.
            $datasout[$i][$k] = substr ($datasout[$i][$k], 0, -strlen($this->_display_separator));

            // Suppression des éléments de textes définis dans entry/search/filter/drop ou entry/search/filter//drop.
            // Si entry/search/filter/drop existe.
            if (isset ($filters['drop']))
            {
              // Si entry/search/filter/drop est un tableau.
              if (is_array ($filters['drop']))
              {
                foreach ($filters['drop'] as $vdrop)
                {
                  $datasout[$i][$k] = str_replace ($vdrop, '', $datasout[$i][$k]);
                }
                unset ($vdrop);
              }
              // Si entry/search/filter/drop une chaine de caractère
              else
              {
                $datasout[$i][$k] = str_replace ($filters['drop'], '', $datasout[$i][$k]);
              }
            }
            // Sinon c'est que entry/search/filter//drop existe
            else
            {
              // si un index pour un entry/search/filter//drop existe
              if (isset ($datas['index']))
              {
                foreach ($filters as $kentry => $ventry)
                {
                  if (isset ($ventry['drop']) && $datas['index'] === $kentry)
                  {
                    // si entry/search/filter//drop est un tableau, traitement pour chaque élément du tableau.
                    if (is_array ($ventry['drop']))
                    {
                      foreach ($ventry['drop'] as $vdrop)
                      {
                        $datasout[$i][$k] = str_replace ($vdrop, '', $datasout[$i][$k]);
                      }
                      unset ($vdrop);
                    }
                    // si c'est une chaine de caractère.
                    else
                    {
                      $datasout[$i][$k] = str_replace ($ventry['drop'], '', $datasout[$i][$k]);
                    }
                  }
                }
                unset ($ventry);
              }
            }
          }
          unset ($vdata);

          if (isset ($template['l']))
          {
            $linksout[$i] = array ('url' => $datas['match'][$template['l']][$i], 'postdatas' => '');
            // si le lien ne commence pas par "http://", différents traitements sont appliqués afin de construire un lien valide
            if (substr ($linksout[$i]['url'], 0, 7) != 'http://')
            {
              // Si le lien commence par le nom de domaine, rajout de "http://"
              if (substr ($linksout[$i]['url'], 0, strlen ($urlelements['domain'])) == $urlelements['domain'])
              {
                $linksout[$i]['url'] = 'http://'.$linksout[$i]['url'];
              }
              // Si le lien commence par un chemin relatif, suppression de tous les "../" et ajout avant de "http://",
              // du nom de domaine puis du chemin pour atteindre la page si présent
              elseif (substr ($linksout[$i]['url'], 0, 3) == '../')
              {
                $linksout[$i]['url'] = 'http://'.$urlelements['domain'].$urlelements['link']['path'].str_replace ('../', '', $linksout[$i]['url']);
              }
              // Si le lien commence par un fichier htm
              elseif (preg_match ('#^[\w]*.[\w]{3,4}#i', $linksout[$i]['url']) !== false)
              {
                $linksout[$i]['url'] = 'http://'.$urlelements['domain'].$urlelements['link']['path'].$linksout[$i]['url'];
              }
              else throw new Exception ('le format du lien "'.$linksout[$i]['url'].'" dans "'.$urlelements['name'].'" n\'est pas pris en charge.');
            }
          }
          else
          {
            $linksout[$i] = $this->makeLinkStructure_TA ($urlelements, array ('references' => $datasout[$i]['reference'], 'keywords' => $keywords, 'numbers' => ($i+1)));
          }
        }
        return array ('number' => $nb_machines, 'datas' => $datasout, 'links' => $linksout);
      }
      else return array ('number' => 0, 'datas' => null, 'links' => null);;
    }
    catch (Exception $e)
    {
      $this->exceptionMsg ($e);
    }
  }

  /*
   * Méthode privée - Transforme les données d'une entrée en un hyperlien HTML valide en données GET et POST.
   *
   * Paramètres :
   * o entry   : Vector - Informations d'une entrée (cf. fonction setEntry).
   * o request : Vector[String] - Eléments nécéssaires à la construction d'un lien
   *             |-> references : String - Numéro d'inventaire.
   *             |-> keywords   : String - Mots-clés de la recherche.
   *             +-> numbers    : Integer - Numéro de la machine dans la recherche.
   *
   * Retourne : Vector[String] - Lien vers les pages des sites
   *            |-> url       : String - Données GET formatées.
   *            +-> postdatas : String - Données POST formatées.
   */
  private function makeLinkStructure_TA (&$entry, $request)
  {
    $link = array ('url' => 'http://'.$entry['domain'].$entry['link']['path'], 'postdatas' => '');
    // Si le champ "get" n'est pas nul
    if ($entry['link']['get'] !== null)
    {
      $link['url'] .= '?';
      foreach ($entry['link']['get'] as $kg => $vg)
      {
        $link['url'] .= $kg.'='.$vg.'&';
      }
      unset ($vg);
      $link['url'] = substr ($link['url'], 0, -1);

      foreach ($this->_patterns as $k => $v)
      {
        $link['url'] = str_replace ($v, $this->extractReference ($request[$k], $entry['link']['filter'][0]['regex']), $link['url']);
      }
      unset ($v);
    }

    // Si le champ "post" n'est pas nul
    if ($entry['link']['post'] !== null)
    {
      foreach ($entry['link']['post'] as $kg => $vg)
      {
        $link['postdatas'] .= $kg.'='.$vg.'&';
      }
      unset ($vg);
      $link['postdatas'] = substr ($link['postdatas'], 0, -1);
      foreach ($this->_patterns as $k => $v)
      {
        $link['postdatas'] = str_replace ($v, $this->extractReference ($request[$k], $entry['link']['filter']['regex']), $link['postdatas']);
      }
      unset ($v);
    }

    return $link;
  }

  /*
   * Méthode privée - Reformate un lien à partir d'une expression rationelle.
   *                  (ex : "ACONIT 51235-00" pour obtenir "51235-00")
   * 
   * Paramètres :
   * o reference : String - Numéro d'inventaire dont on ne veux garder qu'une partie.
   * o regex : String - Expression rationelle à appliquer à "reference".
   *
   * Retourne : String - Si l'expression régulière envoyée n'est pas nulle, alors elle est appliquée à "reference",
   *            puis elle est retournée sinon aucun traitement n'est effectué et "reference" est retournée
   *            telle quelle.
   */
  private function extractReference ($reference, &$regex)
  {
    if ($regex !== null && preg_match ($regex, $reference, $reftab) == 1)
    {
      return $reftab [1];
    }
    else
    {
      return $reference;
    }
  }

}

final class XmlToArray
{
  private $_file;
  private $_xml;

  function __CONSTRUCT ($file = null)
  {
    if ($file !== null)
      $this->useXmlFile ($file);
  }
  
  public function useXmlFile ($file)
  {
    $this->_file = $file;
  }

  public function getArray ()
  {
    if ($this->_file !== null)
    {
      $this->_xml = new domDocument ();
      if ($this->_xml->load ($this->_file))
      {
        // $this->_xml->validate ();

        return $this->readXmlEntries ();
      }
      else echo 'Le fichier "'.$this->_file.'" n\'a pas pu être ouvert.';
    }
    else echo 'Aucun fichier défini.';
  }
  
  /*
   * Méthode privée - Lance la lecture d'un fichier XML
   */
  private function readXmlEntries ()
  {
    $entries = array ();
    foreach ($this->_xml->getElementsByTagName ('entry') as $entry)
    {
      if ($this->hasChild ($entry))
      {
        $key = null;
        $value = array ();
        foreach ($entry->childNodes as $node)
        {
          if ($node->nodeName == 'key')
          {
            $key = $this->addValue ($node);
          }
          elseif ($node->nodeName == 'value')
          {
            // Identification du type d'entrée et lancement du traitement approprié
            if ($this->readAttributeValue ($entry, 'type') == 'ta')
              $value = $this->readXmlEntries_TA ($node);
            elseif ($this->readAttributeValue ($entry, 'type') == 'ws')
              $value = $this->readXmlEntries_WS ($node);
          }
        }
        $entries[$key] = $value;
      }
    }
    
    return $entries;
  }

  /*
   * Méthode privée - Lecture d'un élément <value> de type TextualAnalysis
   */
  private function readXmlEntries_TA ($node)
  {
    if ($this->hasChild ($node))
    {
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'name')
        {
          $value['name'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'infos')
        {
          $value['infos'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'themes')
        {
          $value['themes'] = $this->readThemeElements ($childnode);
        }
        elseif ($childnode->nodeName == 'website')
        {
          $value['website'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'domain')
        {
          $value['domain'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'search')
        {
          $value['search'] = $this->readUrlElements ($childnode);
        }
        elseif ($childnode->nodeName == 'link')
        {
          $value['link'] = $this->readUrlElements ($childnode);
        }
        elseif ($childnode->nodeName == 'disabled')
        {
          if ($childnode->nodeValue == 'true')
            $value['disabled'] = true;
          else
            $value['disabled'] = false;
        }
      }
      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Lecture d'un élément <value> de type WebService
   */
  private function readXmlEntries_WS ($node)
  {
    if ($this->hasChild ($node))
    {
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'name')
        {
          $value['name'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'infos')
        {
          $value['infos'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'themes')
        {
          $value['themes'] = $this->readThemeElements ($childnode);
        }
        elseif ($childnode->nodeName == 'website')
        {
          $value['website'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'domain')
        {
          $value['domain'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'server')
        {
            $value['server'] = $this->readServerElements ($childnode);
        }
        elseif ($childnode->nodeName == 'parameters')
        {
          $value['parameters'] = $this->readParametersElements ($childnode);
        }
        elseif ($childnode->nodeName == 'link')
        {
          $value['link'] = $this->readUrlElements ($childnode);
        }
        elseif ($childnode->nodeName == 'disabled')
        {
          $v = $this->addValue ($childnode);
          if ($v == 'true')
            $value['disabled'] = true;
          else
            $value['disabled'] = false;
        }
      }
      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Ajout du contenu d'un élément XML
   */
  private function addValue ($node)
  {
    return htmlspecialchars_decode ($node->nodeValue, ENT_NOQUOTES);
  }

  /*
   * Méthode privée - Regarde si un élément XML à des enfants
   */
  private function hasChild ($node)
  {
    if ($node->hasChildNodes ())
    {
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeType == XML_ELEMENT_NODE)
        {
          return true;
        }
      }
    }
    return false;
  }

  /*
   * Méthode privée - Lit les valeurs d'un élément <get> ou <post>
   */
  private function readMethodElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      foreach ($node->childNodes as $childnode)
      {
        $attribute = null;

        if ($childnode->nodeName == 'value' && $childnode->hasAttributes ())
        {
          // Lecture des attributs d'un élément
          foreach ($childnode->attributes as $attrnode)
          {
            if ($attrnode->name == 'name')
              $attribute = $attrnode->value;
          }
          unset ($attrnode);
          
          if ($this->hasChild ($childnode))
          {
            $value[$attribute] = array ();
            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'value')
                $value[$attribute][] = $this->addValue ($grandchildnode);
            }
          }
          else
          {
            $value[$attribute] = $this->addValue ($childnode);
          }
        }
      }
      
      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Lit les valeurs d'un élément <filter>
   */
  private function readFilterElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      $i = 0;
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'value')
        {
          $value[$i] = array ();
          if ($this->hasChild ($childnode))
          {
            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'regex')
              {
                $value[$i]['regex'] = $this->addValue ($grandchildnode);
              }
              elseif ($grandchildnode->nodeName == 'template')
              {
                $value[$i]['template'] = $this->addValue ($grandchildnode);
              }
              elseif ($grandchildnode->nodeName == 'drop')
              {
                $value[$i]['drop'] = $this->addValue ($grandchildnode);
              }
            }
            $i ++;
          }
        }
      }
      
      return $value;
    }
    return null;
  }

  /*
   * Méthode privée - Lit les valeurs des éléments <search> et <link>
   */
  private function readUrlElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'path')
        {
          $value['path'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'get')
        {
          $value['get'] = $this->readMethodElements ($childnode);
        }
        elseif ($childnode->nodeName == 'post')
        {
          $value['post'] = $this->readMethodElements ($childnode);
        }
        elseif ($childnode->nodeName == 'filter')
        {
          $value['filter'] = $this->readFilterElements ($childnode);
        }
        elseif ($childnode->nodeName == 'repeat')
        {
          if ($this->hasChild ($childnode))
          {
            $value['repeat'] = array ();
            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'first')
              {
                $value['repeat']['first'] = (int)$this->addValue ($grandchildnode);
              }
              elseif ($grandchildnode->nodeName == 'per_page')
              {
                $value['repeat']['per_page'] = (int)$this->addValue ($grandchildnode);
              }
              elseif ($grandchildnode->nodeName == 'regex')
              {
                $value['repeat']['regex'] = $this->addValue ($grandchildnode);
              }
            }
            unset ($grandchildnode);
          }
          else $value['repeat'] = null;
        }
      }

      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Lit les valeurs d'un élément <theme>
   */
  private function readThemeElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'value')
        {
          $value[] = $this->addValue ($childnode);
        }
      }
      
      return $value;
    }
    return null;
  }

  /*
   * Méthode privée - Lit les valeurs d'un élément <parameters>
   */
  private function readParametersElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'methodname')
        {
          $value['methodname'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'values')
        {
          if ($this->hasChild ($childnode))
          {
            $value['values'] = array ();
            $i = 0;

            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'value' && $this->hasChild ($grandchildnode))
              {
                $value['values'][$i] = array ();
                foreach ($grandchildnode->childNodes as $grandgrandchildnode)
                {
                  /* Ici la balise <value> représente la valeur du paramètre
                   * est la balise <type> son type, généralement "string".
                   */
                  if ($grandgrandchildnode->nodeName == 'value')
                  {
                    $value['values'][$i][$grandgrandchildnode->nodeName] = $this->addValue($grandgrandchildnode);
                  }
                  elseif ($grandgrandchildnode->nodeName == 'type')
                  {
                    $value['values'][$i][$grandgrandchildnode->nodeName] = $this->addValue($grandgrandchildnode);
                  }
                }
                $i ++;
              }
            }
            unset ($i, $grandgrandchildnode, $grandchildnode);
          }
        }
        elseif ($childnode->nodeName == 'elements')
        {
          if ($this->hasChild ($childnode))
          {
            $value['elements'] = array ();
            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'value')
              {
                $attribute = $this->readAttributeValue ($grandchildnode, 'name');

                $value['elements'][$attribute] = $this->addValue ($grandchildnode);
              }
            }
            unset ($attribute, $grandchildnode);
          }
        }
        elseif ($childnode->nodeName == 'resultsname')
        {
          if ($this->hasChild ($childnode))
          {
            $value['resultsname'] = array ();
            foreach ($childnode->childNodes as $grandchildnode)
            {
              if ($grandchildnode->nodeName == 'value')
              {
                $value['resultsname'][] = $this->addValue ($grandchildnode);
              }
            }
            unset ($grandchildnode);
          }
        }
      }

      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Lit les valeurs d'un élément <server>
   */
  private function readServerElements ($node)
  {
    if ($this->hasChild ($node))
    {
      $value = array ();
      foreach ($node->childNodes as $childnode)
      {
        if ($childnode->nodeName == 'path')
        {
          $value['path'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'port')
        {
          $value['port'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'transport')
        {
          $value['transport'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'login')
        {
          $value['login'] = $this->addValue ($childnode);
        }
        elseif ($childnode->nodeName == 'password')
        {
          $value['password'] = $this->addValue ($childnode);
        }
      }

      return $value;
    }
    else return null;
  }

  /*
   * Méthode privée - Lit la valeur d'un attribut particulier d'un élément
   */
  private function readAttributeValue ($node, $attr_name)
  {
    foreach ($node->attributes as $attr_node)
    {
      if ($attr_node->name == $attr_name)
        return $attr_node->value;
    }
    
    return null;
  }
}
?>
