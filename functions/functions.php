<?php
/*
 * Sécurisation des variables superglobales
 */
function secureSuperglobales (&$php_sg)
{
	if (!empty ($php_sg))
	{
		foreach ($php_sg as $k => $v)
		{
			if (is_array ($php_sg[$k]))
			{
				secureSuperglobales ($php_sg[$k]);
			}
			else
			{
				$php_sg[$k] = htmlspecialchars ($v);
			}
		}
		unset ($v);
	}
}

function displayErrorMessages ($error_msgs)
{
  if ($error_msgs != null)
  {
    foreach ($error_msgs as $v)
      echo '<p><strong style="color:red; font-family:arial;">'.$v.'</strong></p>';
      
    unset ($v);
  }
}
?>