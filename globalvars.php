<?php
// V1.1.01	2010-07-24	PhD	Cr�ation (extrait de index.php)

/* Constantes et variables globales
*****************************************************************************/

/* Ce NUM�RO DE VERSION est � mettre � jour � chaque modfication d'un module */
define ('__VERSION__', '1.1.01');
define ('__COMPANY__', 'ACONIT');
define ('__WEB_ADDRESS__', 'http://www.aconit.org/');
define ('__TRACE__', false);

/* TRACE - timer */ if (__TRACE__) {$timestart = microtime(true);}

// Application de la zone horaire Europe/Paris (PHP 5 et sup)
if (function_exists ('date_default_timezone_set'))
{
  date_default_timezone_set('Europe/Paris');
}

// D�finition de la localit�
setlocale(LC_ALL, array('fr_FR.UTF8', 'fr_FR@euro'));

// Historique de l'application 
$hist = array('version' => __VERSION__,
              'company' => __COMPANY__,
              'web_address' => __WEB_ADDRESS__,
              'date'    => date ("d/m/Y", filemtime(__FILE__)),
              'time'    => date ('H:i', filemtime(__FILE__))
             );

/* En-t�te HTML
*****************************************************************************/
$html_head ='
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="styles/main.css" />
';

?>