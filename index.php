<?php
// V1.1.00	2010-07-22	PhD	Corrections appels images, Repris texte d'aide.
//							Modifié nom de thème "Sciences et Techniques", 
//							Ajout lien "Notes"
// V1.1.01	2010-07-24	PhD	Extrait les varaibles globals -> globalvars.php


/* Inclusions externes ***************************************************************************/
require_once ('./globalvars.php');
require_once ('./functions/functions.php');
require_once ('./functions/class/Portal.php');

/* Déclaration et initialisation des variables ***************************************************/
$post = &$_POST;
$get = &$_GET;

// Messages d'erreur
$error = array('form_empty' => 'Champ(s) vide(s) dans le formulaire',
               'form_wrong_value' => 'Donnée(s) erronée(s) dans une zone du formulaire'
              );

/* Appel des fonctions de traitement *************************************************************/
secureSuperglobales ($post);
secureSuperglobales ($get);

session_start (); // Ne rien afficher avant


/* Code xHTML ************************************************************************************/

// Redirection de lien
if (isset ($get['basename'], $get['link']['url'], $get['link']['postdatas']))
{
  $url = utf8_decode (html_entity_decode ($get['link']['url']));
  $postdatas = utf8_decode (html_entity_decode ($get['link']['postdatas']));
	// Redirection vers la page voulue dans l'encodage ISO-8859-1
	if ($postdatas == null)
	{
		header ('location:'.$url);
    exit ();
	}
	else
	{
  	$ch = curl_init ();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_POST, true);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdatas);
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt ($ch, CURLOPT_MAXREDIRS, 2);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, false);
    curl_exec ($ch);
    curl_close ($ch);
  }
}
// Page de recherche
else
{
	echo $html_head;		// En-tête HTML et lignes fixes du bloc "head"
	echo '
		<title>Portail Multi Accès v'.$hist['version'].' - ACONIT</title>
		<script type="text/javascript" src="scripts/script.js"></script>
	</head>

	<body onload="initDocument ();">';
  
  /* Bloc [loading] ******************************************************************************/
  echo '<div id="loading"><div class="loading_info"><p><img src="pictures/loading.gif" alt="Image de chargement" /> Recherche en cours...</p></div></div><!-- Fin du bloc [loading] -->'."\n";

  /* Initialisation des objets de recherche ******************************************************/
  $portal = new Portal_Main ();
  $portal->init ();

  // Initialisation de la variable "keys" (index d'une ou plusieurs entrée(s))
  if (isset ($post['keys']))
    $keys = $post['keys'];
  else
    $keys = null;

  /* Bloc [header] *******************************************************************************/
	echo '   <div id="header">
    <div class="logo"><a href="'.__WEB_ADDRESS__.'"><img src="./pictures/logo.png" height="77px" width="144px" alt="Logo" /></a></div>
		<div class="version"><p>Version '.$hist['version'].', modifiée le '.$hist['date'].' à '.$hist['time'].' - Tous droits réservés <a href="'.$hist['web_address'].'">'.$hist['company'].'</a></p></div>
		</div><!-- Fin du bloc [header] -->'."\n";

  echo '<div id="content">'."\n";

  /* Bloc [Search] *******************************************************************************/
  echo '<div id="bloc_search">
    <div id="search_frame">
      <div id="help"><img src="pictures/help_trans.gif" alt="Aide" title="Aide" onmouseover="setVisible (\'help_frame\', true);" onmouseout="setVisible (\'help_frame\', false);" /></div>
      <div id="help_frame">
        <h1>Aide</h1>
        <ol>
          <li>Choisir la ou les base(s) que vous voulez consulter. Cocher leur case.
          <br/>Vous pouvez aussi faire un choix global par «&nbsp;Thème&nbsp».</li>
          <li>Entrer un ou plusieurs mots caractérisants les objets que vous cherchez.</li>
          <li>Cliquer sur «&nbsp;Rechercher&nbsp;». 
          <br/>Patientez&nbsp;: le temps de recherche dépend du nombre de bases sélectionnées&nbsp;!</li>
          <li>Les caractéristiques essentielles des objets obtenus s\'affichent, ainsi qu\'un lien vers leur fiche dans leur site web respectif.</li>
        </ol>
        <p><img src="pictures/warning.gif" alt="Attention" /> <strong>  « Note technique »  : </strong>Voir lien en bas de page.</p>
      </div><!-- Fin bloc "help_frame" -->
      <div id="entryinfos">
        <div id="entryinfos_body">
          <div id="entryinfos_row1">
            <div id="entryinfos_cell1"></div>
            <div id="entryinfos_cell2"></div>
            <div id="entryinfos_cell3"></div>
          </div>
          <div id="entryinfos_row2">
            <div id="entryinfos_cell4"></div>
            <div id="entryinfos_cell5">
              <div id="entryinfos_text"></div>
            </div>
            <div id="entryinfos_cell6"></div>
          </div>
          <div id="entryinfos_row3">
            <div id="entryinfos_cell7"></div>
            <div id="entryinfos_cell8"></div>
            <div id="entryinfos_cell9"></div>
          </div>
        </div>
      </div><!-- Fin bloc [entryinfos] -->
      <div class="search_frame_up"><h1 class="search_frame_title">Recherche</h1></div>

      <div class="search_frame_middle">
        <form action="" method="post" id="choice" onsubmit="receiveContent (this); return false;">
          <fieldset class="search_frame_content">
            <legend>Choix des bases';
// Affichage du rapport entrées actives sur entrées totales
//echo '            - '.$portal->getNumberOfActiveEntries ().'/'.$portal->getNumberOfEntries ().' d\'active(s)';
echo '            </legend>
            <div id="form_domain">
              <label for="domain">Thème</label>
              <select id="domain" onchange="actionList (document.getElementById (\'choice\'), this);">
                <option value="none">-Aucun-</option>
                <option value="informatique">Informatique</option>
                <option value="pstc">Sciences et Techniques</option>
              </select>
            </div>

            <div id="form_chekboxes">'."\n";

  // Affichage des cases à cocher de chaque entrée
  foreach ($portal->getKeys () as $k)
	{
		echo '					  <input type="checkbox" name="keys[]" value="'.$k.'" id="'.$k.'" ';
		if ($portal->getEntryState ($k)) echo 'disabled="disabled" ';
    elseif (!$portal->getEntryState ($k) && ($keys === null) || current ($keys) === $k)
    {
      echo 'checked="checked" ';
      if ($keys !== null)
        next ($keys);
    }
		echo ' onmousemove="moveBlock (this, \'entryinfos\', event, \'y\');" onmouseover="setVisible (\'entryinfos\', true); httpDatas (entriesinfos[\''.$k.'\'][\'infos\'], \'entryinfos_text\');" onmouseout="setVisible (\'entryinfos\', false);" />'."\n";
    echo '           <label for ="'.$k.'" onmousemove="moveBlock (this, \'entryinfos\', event, \'y\');" onmouseover="setVisible (\'entryinfos\', true); httpDatas (entriesinfos[\''.$k.'\'][\'infos\'], \'entryinfos_text\');" onmouseout="setVisible (\'entryinfos\', false);">'.$portal->getEntryName ($k).'</label><br />'."\n";
  }
	unset ($v);

  echo '                </div>
              <div id="form_checkboxall">
                <input type="checkbox" name="keys[]" value="all" id="all" onclick="checkTheBoxes (document.getElementById (\'choice\'), \'all\');" />
                <label for="all"><b>Tout cocher/décocher</b></label>
              </div>
            </fieldset>
            <div id="launchsearch">
              <label for="request">Recherche </label><input type="text" name="request" id="request"';
  if (isset ($post['request'])) echo 'value="'.$post['request'].'"';
  echo ' />
              <input type="submit" value="Rechercher" />
            </div>
          </form>

        </div>
        <div class="search_frame_down"></div>
      </div><!-- Fin de bloc[search_frame] -->
		</div><!-- Fin du bloc [search] -->'."\n";


  /* Bloc [result] *******************************************************************************/
	echo '		<div id="bloc_result">'."\n";
    /* Contenu du résultat */
  if (isset ($post['keys'], $post['request']))
	{
    include ('result_content.php');
  }
	echo '		</div><!-- Fin du bloc [result] -->'."\n";

  echo '</div><!-- Fin du bloc [content] -->'."\n";

  /* Bloc [trace] ********************************************************************************/
 	if (__TRACE__)
	{
		echo '		<div id="bloc_trace">'."\n";
		// TRACE - post
    echo '			<p>&gt; Contenu POST : ';
    print_r ($post);
    echo '			</p>'."\n";

    // TRACE - timer
    $timestop = microtime (true);
    echo '			<p>&gt; Temps de chargement : '.round($timestop-$timestart,3).' s</p>'."\n";

		// TRACE - js
    echo '			<script type="text/javascript"><!--'."\n".'document.write(\'<p>&gt; JavaScript activé</p>\');'."\n".'// --></script><noscript><p>TRACE =&gt; JavaScript désactivé</p></noscript>'."\n";

    // TRACE - error messages
    echo '			<p>&gt; Messages d\'erreurs : ';
    displayErrorMessages ($portal->getErrorMessages ());
    echo '			</p>'."\n";

		echo '		</div><!-- Fin du bloc [trace] -->'."\n";
	}
  
  /* Bloc de préchargement des images de rollover ************************************************/
  echo'   <!-- Préchargement des images de rollover -->
    <div style="display:none;">
      <img src="pictures/up_hover.png" alt="" />
      <img src="pictures/help_on.png" alt="" />
      <img src="pictures/magnify_hover.png" alt="" />
      <!-- Préchargement des images de fond de bloc -->
      <img src="pictures/loading_bg.png" alt="" />
      <img src="pictures/bgsearch.png" alt="" />
    </div>'."\n";
    
  /* Bloc [footer] *******************************************************************************/
  echo '    <div id="footer">
      <div class="footer_infos">Portail Multi Accès - ACONIT 2010 - A. Adamy</div>
      <div class="footer_logow3c"><img src="pictures/valid_xhtml11.gif" alt="Valide xhtml 1.0"/><img src="pictures/valid_css.gif" alt="Valide CSS"/></div>
      <div class="footer_notes">Voir <a href=notes.php>«&nbsp;Notes techniques&nbsp;»</a></div>
    </div><!-- Fin du bloc [footer] -->
  </body>
</html>'."\n";
}

?>