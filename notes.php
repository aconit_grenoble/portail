<?php
// V1.1			2010-07-22	PhD	Création
// V1.1.01		2010-07-24	PhD	Ajout appel globalvars

/* Inclusions externes ***************************************************************************/
require_once ('./globalvars.php');

echo $html_head;		// En-tête HTML et lignes fixes du bloc "head"
echo <<<EOD
		<title>Portail Multi Accès - Notes techniques</title>
	</head>
EOD;
?>

<body>

<!-- Bloc [header] ***************************************************************************** -->
<div id="header_notes">
    <div class="logo">
    	<a href="http://www.aconit.org"><img src="./pictures/logo.png" height="77px" width="144px" alt="Logo" /></a>
    </div>
    <div id="header_notes_titre"> Portail Multi Accès : Notes techniques</div>
	
</div><!-- Fin du bloc [header] -->

<div id="notes">
	<h3> Chacune des bases consultées a ses propres techniques d'interrogation… <strong>Portail Multi Accès</strong> est conçu pour fournir un maximum de réponses, les plus homogènes possibles&nbsp;!
		</br> Les notes ci-dessous précisent quelques particularités et quelques règles de traitement.
	</h3>

	<h1>1. Nature des objets</h1>
	<ul>
		<li>À ce jour, le portail ne cherche que les réponses de type objets "Machines" et non pas les "Documents" et les "Logiciels". Ceci est fait pour obtenir des résultats homogènes, plusieurs bases ne traitant que les machines, ou nécessitant une interrogation spéciale pour rechercher les documents.
			<br/>Bien noter qu'il est toujours possible, après avoir consulté une fiche sur un site, de mener d'autres recherches approfondies directement sur ce site. 
		</li>
	</ul>


	<h1>2. Champ de recherche</h1>
	<ul>
		<li>La recherche est effectuée sur une chaine de caractères unique : c'est à dire que si on fournit plusieurs mots (ex: "lecteur cartes") c'est l'<strong>ensemble</strong> de ces mots qui est recherché, espaces compris.
			<br/>Pour les (rares) bases effectuant une recherche en "OU" sur les différents mots, un post traitement est effectué pour ne conserver que les réponses cohérentes.</li>
		<li>Certaines bases séparent la marque ("IBM" par exemple) du nom du modèle ("1130" par exemple). Une recherche cumulant les 2 éléments peut échouer.
		<li>Les recherches sont faites chaque fois que posible en "plein texte", mais certains sites n'autorisent que des recherches sur des critères/champs limités.</li>
	</ul>
		
	<h1>3. Thèmes</h1>
	<ul>
		<li><strong>Informatique : </strong>Ce thème sélectionne les organismes qui possèdent une collection "réelle" de machines informatiques. Le portail ne traite pas (à ce jour) les sites qui ont "seulement" des bases de données de photos et renseignements techniques ou historiques, souvent fort bien documentées.</li>
		<li><strong>Sciences et Technique : </strong>il s'agit ici d'une sélection des sites qui participent au Programme National de Sauvegarde du Patrimoine Scientifique et Technique Contemporain. 
		<br/>(voir le site <a src="http://wwww.patstec.fr">http://wwww.patstec.fr</a>)
		<br/>Le programme a pour mission d'identifier dans chaque région, dans les universités, centre de recherche et centres industriels, tous les objets contemporains (50 ans) susceptibles d'être conservés au titre du patrimoine.</li>
	</ul>
		
	<h1>4. Particularités des sites</h1>
	<p style="font-weight:italic; margin-left: 200px;"> <strong>Note pour les organismes concernés : </strong>Si une information est erronée, ou si vous voulez compléter ces informations, nous sommes prêts à mettre à jour&nbsp;!</p>
	<ul>
		<li><strong>ACONIT : </strong>	Les 2 bases sont interrogées par un interface web-service XML-RPC, permettant un accès rapide et efficace.</li>
		<li> <strong>CNAM - Musée des Arts et Métiers : </strong>Les réponses peuvent très lentes, en particulier si le site donne beaucoup de réponses (nécessité de plusieurs appels successifs).</li>
		<li><strong>Fédération des Équipes Bull : </strong>L'interrogaton "full-texte" n'est pas possible. Le portail n'utilise qu'un nombre limité des critères possibles pour essayer d'obtenir des réponses exhaustives.</i>
		<li><strong>Musée Lecoq : </strong></li>
		<li><strong>MO5 : </strong></li>
		<li><strong>PATSTEC :  </strong> Il s'agit de la base de données "publique" du programme national. La base de données du site PATSTEC contient donc des copies de fiches créées dans les régions (ACONIT...)</li>
		<li><strong>WDA :  </strong></li> 
		
	<h1>5. Rejoignez le Portail Multi Accès&nbsp;!</h1>
	<h3> Si votre organisation (publique, privée, associative) gère une base de données qui concerne le patrimoine scientifique et technique, n'hésitez pas à nous contacter. Nous étudierons ensemble comment l'intégrer au Portail Multi Accès. 
	<br/> Plus le nombre de sites analysés sera important, plus les recherches seront efficaces et plus vous serez reconnus&nbsp;!
	<br/> Contactez le <a href='http://www.aconit.org/spip/auteur.php3?id_auteur=9'>webmestre ACONIT</a></h3>
</div><!-- Fin du bloc [notes] -->

<div id="footer">
      <div class="footer_notes">Retour : <a href=index.php>Portail Multi Accès ACONIT</a></div>
    </div><!-- Fin du bloc [footer] -->
</body>
</html>
