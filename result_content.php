<?php
  /* Initialisation des objets de recherche ******************************************************/
  if (!isset ($portal))
  {
    /* Inclusion des fonctions *******************************************************************/
    require_once ('./functions/functions.php');
    /* Inclusion des classes *********************************************************************/
    require_once ('./functions/class/Portal.php');

    /* Initialisation des objets de recherche ****************************************************/
    $portal = new Portal_Main ();
    $portal->init ();
  }

  $post = &$_POST;
  secureSuperglobales ($post);

  if (isset ($post['keys']) && $post['keys'] && !empty ($post['request']))
  {
    /* Appel de la méthode de recherche de Multibase */
    $results = $portal->search ($post['keys'], $post['request']);
    if ($results)
    {
      foreach ($results as $key => $result)
      {
        echo '			<div id="title_'.$key.'" class="rslt_ttl">
                      <div class="rslt_ttl_pm"><img src="pictures/minus.png" alt="afficher/cacher" id="plusminus_'.$key.'" style="cursor:pointer;" onclick="displayBlock (\'result_'.$key.'\', -1); switchPicture (\'plusminus_'.$key.'\', \'plus.png\', \'minus.png\')" /></div>
                      <div class="rslt_ttl_link"><a href="'.$portal->getEntryWebSite ($key).'" target="_blank">'.$portal->getEntryName ($key).'</a></div>
                      <div class="rslt_ttl_nbres">'.$result['result_number'].' résultat(s)</div>
                    </div><!-- Fin du bloc [title_'.$key.'] -->
        <div id="result_'.$key.'" class="result_body">
          <table>
          <tbody>
              <tr>
                <th width="20%">N° d\'inventaire</th><th width="10%">Type</th><th width="65%">Description courte</th><th width="5%">Fiche</th>
              </tr>'."\n";
              
          for ($i=0; $i < $result['result_number']; $i++)
          {
            echo '					  <tr class="'.($i%2 ? 'rslt_lgn1' : 'rslt_lgn2').'">
                <td>'.$result['datas'][$i]['reference'].'</td>
                <td>'.$result['datas'][$i]['type'].'</td>
                <td><a href="index.php?'.htmlentities ('basename='.$key.'&link[url]='.urlencode ($result['links'][$i]['url']).'&link[postdatas]='.urlencode ($result['links'][$i]['postdatas'])).'" target="_blank">'.$result['datas'][$i]['name'].'</a></td>
                <td><a href="index.php?'.htmlentities ('basename='.$key.'&link[url]='.urlencode ($result['links'][$i]['url']).'&link[postdatas]='.urlencode ($result['links'][$i]['postdatas'])).'" target="_blank"><div class="magnify"></div></a></td>
              </tr>'."\n";
          }
          echo '          </tbody>
        </table>
      </div>
      <div>
        <p><a href="#header" title="Haut"><div class="up"></div></a></p>
      </div>'."\n";
      }
      unset ($result);
    }
  }
  else
  {
    echo '			<div id="errormsg">
        <table><tbody><tr><td width="50px" height="50px"><img src="pictures/errormsg.png" /></td><td><b>Recherche impossible :</b> au moins une base doit être sélectionnée et le champ de recherche ne doit pas être vide</td></tr></tbody></table>
      </div><!-- Fin du bloc [errormsg] -->'."\n";
  }
?>