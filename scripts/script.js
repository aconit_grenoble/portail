/*
 * Variables globales
 */
// Variable contenant les informations sur les entrées
var entriesinfos = new Array ();

/*
 * Coche ou décoche un groupe de cases à cocher en fonction d'un thème
 */
function checkTheBoxes (form, theme)
{
	// Coche ou décoche toutes les cases des bases actives
	for (var i = 0; i < form.length; i ++)
	{
    if (theme == 'all')
    {
      if (form[i].type == 'checkbox' && form[i].value != 'all' && !form[i].disabled)
      {
        form[i].checked = form['all'].checked;
      }
    }
    else
    {
      form[i].checked = false;
      if (form[i].type == 'checkbox' && form[i].value != 'all' && !form[i].disabled)
      {
        for (var j = 0; j < entriesinfos[form[i].value]['theme'].length; j ++)
        {
          if (theme == entriesinfos[form[i].value]['theme'][j])
          {
            form[i].checked = true;
          }
        }
      }
    }
  }
}

/*
 * Interverti deux images de "/pictures/" lors d'une action
 */
function switchPicture (id, picture1, picture2)
{
  var elmnt = document.getElementById (id);
  if (elmnt.src == 'http://'+document.location.host+'/pictures/'+picture1)
    elmnt.src = 'pictures/'+picture2;
  else
    elmnt.src = 'pictures/'+picture1;
}

/*
 * Repli le contenu d'un résultat d'une recherche et change l'image du bouton de repli
 */
function displayBlock (id, state)
{
  var block = document.getElementById (id);
  if (state == -1)
  {
    if (block.style.display == 'none')
      block.style.display = 'block';
    else
      block.style.display = 'none';
  }
  else if (state == 1)
  {
    block.style.display = 'block';
  }
  else
  {
    block.style.display = 'none';
  }
}

/*
 * Affiche ou cache un bloc HTML
 */
function setVisible (id, state)
{
  var frame = document.getElementById (id);
  if (state)
    frame.style.visibility = 'visible';
  else
    frame.style.visibility = 'hidden';
}


/*
 * Requête HTTP vers un serveur pour obtenir les rélustats d'une recherche
 *
 * Paramètres :
 * o callback : fonction de rappel
 * o display
 * o address
 * o param
 * o loadingstate
 */
function httpRequest (callback, display, address, param, id, loadingstate)
{
  if (display)
  {
    var xhr_object = null; 

    if (window.XMLHttpRequest) // Firefox
    {
      xhr_object = new XMLHttpRequest ();
    }
    else if (window.ActiveXObject) // Internet Explorer
    {
      xhr_object = new ActiveXObject ('Microsoft.XMLHTTP');
    }
    else // XMLHttpRequest non supporté par le navigateur
    {
      alert ("Votre navigateur ne supporte pas les objets XMLHttpRequest..."); 
      return; 
    } 

    xhr_object.onreadystatechange = function()
    {
      // Vérification que le serveur à terminé le traitement
      if (xhr_object.readyState == 4 && (xhr_object.status == 200 || xhr_object.status == 0))
      {
        callback (xhr_object.responseText, id);
        if (loadingstate === true)
        {
          setVisible ('loading', false);
        }
      }
      else if (xhr_object.readyState < 4)
      {
        if (loadingstate == true)
        {
          setVisible ('loading', true);
        }
      }
    };
    
    xhr_object.open('POST', './'+address, true);
    xhr_object.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr_object.send (param);
  }
  else callback ('', id);
}

/*
 * Insertion d'un contenu dans un élément HTML
 *
 * Paramètres :
 * o datas : code HTML à afficher
 * o id    : bloc dans lequel insérer les données
 */
function httpDatas (datas, id)
{
    document.getElementById(id).innerHTML = datas;
}

/*
 * Requête HTTP vers un serveur pour obtenir les informations sur les bases
 *
 * Paramètres :
 * o address
 * o param
 */
function xmlRequest (address)
{
    var xhrobj = null; 

    if (window.XMLHttpRequest) // Firefox
    {
      xhrobj = new XMLHttpRequest ();
    }
    else if (window.ActiveXObject) // Internet Explorer version 6 et inférieures
    {
      xhrobj = new ActiveXObject ('Microsoft.XMLHTTP');
    }
    else // XMLHttpRequest non supporté par le navigateur
    {
      alert ("Votre navigateur ne supporte pas les objets XMLHttpRequest..."); 
      return; 
    } 

    xhrobj.onreadystatechange = function()
    {
      // Vérification que le serveur à terminé le traitement
      if (xhrobj.readyState == 4 && (xhrobj.status == 200 || xhrobj.status == 0))
      {
        // Extraction des données
        var xmlresponse = xhrobj.responseXML;
        if (xmlresponse != null)
        {
          var xml = xmlresponse.documentElement;
          var entries = xml.getElementsByTagName ('entry');
          var result = new Array ();

          for (var i = 0; i < entries.length; i ++)
          {
            var entry = entries[i];
            var key = null;

            // attributes.length > 0 à la place de hasAttributes () pour compatibilité IE
            if (entry.attributes.length > 0 && entry.hasChildNodes ())
            {
              for(var j = 0; j < entry.attributes.length; j ++)
              {
                if (entry.attributes[j].nodeName == 'key' )
                {
                  key = entry.attributes[j].nodeValue;
                  result[key] = new Array ();
                }
              }

              for (var j = 0; j < entry.childNodes.length; j ++)
              {
                var node = entry.childNodes[j];
                if (node.nodeType == 1)
                {
                  if (node.nodeName == 'infos')
                  {
                    result[key]['infos'] = node.childNodes[0].nodeValue;
                  }
                  else if (node.nodeName == 'theme')
                  {
                    result[key]['theme'] = new Array ();
                    var index = 0;

                    for (var k = 0; k < node.childNodes.length; k ++)
                    {
                      var childnode = node.childNodes[k];
                      
                      if (childnode.nodeType == 1)
                      {
                        if (childnode.nodeName == 'value')
                        {
                          result[key]['theme'][index] = childnode.childNodes[0].nodeValue;
                          index ++;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          entriesinfos = result;
        }
      }
    };

    xhrobj.open('GET', './'+address, true);
    xhrobj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhrobj.send (null);
}

/*
 * Déplacement d'un bloc d'id "id" sur l'axe X, Y ou les deux
 */
function moveBlock (current, dest, event, move)
{
  var destel = document.getElementById (dest);

  var poscurrent = absolutePos (current);

  if (move == 'x')
  {
    destel.style.marginLeft = poscurrent[0] + 'px';
  }
  else if (move == 'y')
  {
    destel.style.marginTop = poscurrent[1] - 150 + 'px';
  }
  else if (move == 'both')
  {
    destel.style.marginLeft = poscurrent[0] + 'px';
    destel.style.marginTop = poscurrent[1] + 'px';
  }
  else alert ('moveBlock : Valeur incorrecte pour "move"');
}

/*
 * Renvoi la position absolu d'un élément en pixel
 */
function absolutePos (element)
{
	var posx = posy = 0;
  do
	{
		posx += element.offsetLeft;
		posy += element.offsetTop;
		element = element.offsetParent;
	} while (element && element.style.position != 'absolute');
  
  return new Array (posx, posy);
}
 
/*
 * Réception des résultats d'une recherche
 */
function receiveContent (form)
{
  param = '';
	for (var i = 0; i < form.length; i ++)
	{
    if ((form[i].type == 'checkbox' && form[i].checked) || form[i].type == 'text')
    {
      param += form[i].name+'='+form[i].value+'&';
    }
  }
  param = param.substring (0, (param.length-1));
  httpRequest (httpDatas, true, 'result_content.php', param, 'bloc_result', true);
}

/*
 * Action à effectuer lors du chargement de la page
 */
function initDocument ()
{
  displayBlock ('form_checkboxall', 1);
  displayBlock ('form_domain', 1);
  displayBlock ('help', 1);
  document.getElementById ('all').checked = 'checked';
  xmlRequest ('./entries_infos.php', null);
}

/*
 * Défini le comportement des options d'une liste déroulante sur des cases à cocher
 */
function actionList (form, list)
{
	index = list.selectedIndex;
	if (index != 0)
  {
    checkTheBoxes (form, list.options[index].value);
  }
}
